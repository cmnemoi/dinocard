<?php

namespace App\Challenge\Service;

use App\Challenge\Entity\Card;
use App\Challenge\Entity\Charm;
use App\Challenge\Entity\Duel;
use App\Challenge\Entity\Dinoz;
use App\Challenge\Entity\Opponent;
use App\Challenge\Enum\ActionType;
use App\Challenge\Enum\CardType;
use App\Challenge\Enum\DeckPreset;
use App\Challenge\Enum\DinocardStat;
use App\Challenge\Enum\ElementType;
use App\Challenge\Enum\Keyword;
use App\Challenge\Enum\RoundPhase;
use App\Entity\DeckDB;

class DuelSimulator
{
    private OpponentFactory $opponentFactory;
    private EffectInterpreter $effectInterpreter;
    private LogService $logService;

    /** @var Duel|null Current duel */
    private ?Duel $duel = null;
    private int $randomSeed;
    private string $fightUrl;
    private RoundPhase $currentRoundPhase;

    /**
     * @param OpponentFactory   $opponentFactory
     * @param EffectInterpreter $effectInterpreter
     * @param LogService        $logService
     */
    public function __construct(OpponentFactory $opponentFactory, EffectInterpreter $effectInterpreter, LogService $logService)
    {
        $this->opponentFactory = $opponentFactory;

        // Made like this to avoid circular dependency during dependency injection
        $this->effectInterpreter = $effectInterpreter;
        $this->effectInterpreter->setDuelSimulator($this);

        // Made like this to avoid circular dependency during dependency injection
        $this->logService = $logService;
        $this->logService->setDuelSimulator($this);
    }

    /**
     * @return Duel
     */
    public function getDuel(): Duel
    {
        return $this->duel;
    }

    /**
     * @return array
     */
    public function getLogs() : array
    {
        return $this->logService->getLogs();
    }

    /**
     * Prepare an opponent with the specified Deck from DB or create an empty opponent.
     *
     * @param string      $name
     * @param DeckDB|null $deck
     *
     * @return Opponent
     */
    public function prepareOpponentWithDeck(string $name, ?DeckDB $deck): Opponent
    {
        if (null !== $deck) {
            $opponent = $this->opponentFactory->loadOpponentFromDeck($deck);
        } else {
            $opponent = $this->opponentFactory->generateOpponent($name, [], 0);
        }

        return $opponent;
    }

    /**
     * Prepare an opponent with the specified DeckPreset or create an empty opponent.
     *
     * @param string          $name
     * @param DeckPreset|null $deckPreset
     *
     * @return Opponent
     */
    public function prepareOpponentWithPreset(string $name, ?DeckPreset $deckPreset = null): Opponent
    {
        if (null !== $deckPreset) {
            $cardList = $deckPreset->deckList();
            $deckSize = $deckPreset === DeckPreset::Empty ? 0 : 20;
            $opponent = $this->opponentFactory->generateOpponent($name, $cardList, $deckSize);
        } else {
            $opponent = $this->opponentFactory->generateOpponent($name, [], 0);
        }

        return $opponent;
    }

    /**
     * Setup the two opponents that will fight each other.
     *
     * @param Opponent $opponentA
     * @param Opponent $opponentB
     *
     * @return void
     */
    public function prepareFight(Opponent $opponentA, Opponent $opponentB): void
    {
        $this->duel = new Duel($opponentA, $opponentB);
    }

    /**
     * Run the fight simulation between the two opponents
     */
    public function simulateFight(): void
    {
        if ($this->duel === null) {
            dump("No fight data");

            return;
        }

        $this->duel->startFight();
        $this->currentRoundPhase = RoundPhase::Start;

        while (!$this->duel->isFinished()) {
            $this->executeRound();
        }

        $this->logService->addWinnerLog();
        $this->logService->addShareableFightLink($this->fightUrl);
    }

    /**
     * Try to execute both player turns then all attacks
     */
    private function executeRound(): void
    {
        if (!$this->duel->isFinished()) {
            if (RoundPhase::Start === $this->currentRoundPhase) {
                $this->logService->addRoundStartLog();
            } elseif (RoundPhase::OpponentA === $this->currentRoundPhase || RoundPhase::OpponentB === $this->currentRoundPhase) {
                $this->duel->cycleCurrentOpponent();
                $this->playOpponentTurn();
            } elseif (RoundPhase::Attack === $this->currentRoundPhase) {
                $this->attackPhase();
            } elseif (RoundPhase::End === $this->currentRoundPhase) {
                $this->logService->logRoundEnd();
                $this->duel->incrementTurnCount();
            }
            // TODO Be able to skip the attack phase (or even double a phase (such as the Time Warp card in Hearthstone)) !
            $this->currentRoundPhase = RoundPhase::nextPhase($this->currentRoundPhase);
        }
    }

    private function playOpponentTurn(): void
    {
        $this->logService->addPlayerLog("is now playing!");
        $this->startTurnPhase();
        $this->eclosionMarkerPhase();
        $this->drawPhase();
        $this->endTurnPhase();
        $this->logService->addBlankLog();
    }

    private function startTurnPhase(): void
    {
        $this->logService->addTranslatedLog('FightLogs', 'phase.start');

        $this->verifyEternalCardsRelevancy();

        // TODO Trigger all Start Turn Effect in play of this player

        foreach ($this->duel->getCurrentOpponent()->getBoard()->getDinoz() as $boardIndex => $dinoz) {
            $dinoz->modifyStat(DinocardStat::Age, 1);

            if (!$dinoz->isEgg()) {
                $dinozCard = $dinoz->getOwningCard();
                if ($dinozCard->hasKeyword(Keyword::Poisoned)) {
                    $this->logService->addCapacityLog(Keyword::Poisoned, [$boardIndex + 1], null);
                    $dinoz->applyDamage(1);
                }
                if ($dinozCard->hasKeyword(Keyword::Regeneration) && $dinoz->getInjureMarkerCount() > 0) {
                    $this->logService->addCapacityLog(Keyword::Regeneration, [$boardIndex + 1], null);
                    $dinoz->modifyInjureMarker(-1);
                }
                if ($dinozCard->hasKeyword(Keyword::Ephemeral)) {
                    // TODO Find a better way to say that a dinoz dies without taking damages or whatever
                    $this->logService->addCapacityLog(Keyword::Ephemeral, [$boardIndex + 1], null);
                    $dinoz->setInjureMarkerCount($dinoz->getStat(DinocardStat::Endurance));
                }
            }

            $this->verifyDeadDinoz($this->getDuel()->getCurrentOpponent());
        }
    }

    private function verifyEternalCardsRelevancy(): void
    {
        // Required amount of cards above an eternal card to "resurect" the card.
        $requiredAmountToResurrect = 3;
        $currentOpponent = $this->duel->getCurrentOpponent();

        /** @var Card $card */
        $eternalCards = $currentOpponent->filterGraveyardCards(fn($card) => $card->hasKeyword(Keyword::Eternal));
        $graveyardCount = count($currentOpponent->getGraveyardCards());

        if ($graveyardCount > 0) {
            foreach ($eternalCards as $index => $card) {
                if ($index < $graveyardCount - $requiredAmountToResurrect) {
                    $this->logService->addOldCapacityLog(Keyword::Eternal, sprintf("Card %d came back from graveyard", $card->getId()));
                    $currentOpponent->removeCardFromGraveyard($index);

                    // TODO We should maybe rename "suceedToDraw"?
                    $this->succeedToDraw($card);
                }
            }
        }
    }

    /**
     * Just add a hatch marker on all Dinoz Eggs.
     */
    private function eclosionMarkerPhase(): void
    {
        $this->logService->addTranslatedLog('FightLogs', 'phase.hatchMarker');

        $count = 0;
        foreach ($this->duel->getCurrentOpponent()->getBoard()->getEggs() as $egg) {
            $count++;
            $egg->modifyHatchMarker(1);
            $this->logService->addHatchStatusLog($egg);
        }

        if (0 === $count) {
            $this->logService->addLog("...");
        }
    }

    /**
     * Try to draw a card from the deck THEN play it
     */
    private function drawPhase(): void
    {
        $this->logService->logEnergyReserve();
        $this->logService->addTranslatedLog('FightLogs', 'phase.draw');

        $this->attemptCardDraw($this->duel->getCurrentOpponent());
    }

    /**
     * If the attempt succeed, we'll try to play the card
     * @param Opponent $opponent
     *
     * @return bool
     */
    public function attemptCardDraw(Opponent $opponent): bool
    {
        /** @var ?Card $card */
        $card = $opponent->getDeck()->drawCard();
        $drawSucceed = $card !== null;
        if ($drawSucceed) {
            $this->succeedToDraw($card);
        } else {
            $this->failToDraw();
        }

        return $drawSucceed;
    }

    /**
     * Check if we can play the card before playing it. Else put in the Energy Reserve
     * @param Card $card
     */
    private function succeedToDraw(Card $card): void
    {
        $hasEnoughEnergy = $this->duel->getCurrentOpponent()->hasEnoughEnergy($card);
        $hasValidTarget = $this->checkIfATargetExists($card);

        $canPlayCard = $hasEnoughEnergy && $hasValidTarget;
        if ($canPlayCard) {
            try {
                $this->playCard($card);
            } catch (\Exception $e) {
                $this->logService->addErrorLog("[playCard] ".$e->getMessage());
            }
        } else {
            $this->duel->getCurrentOpponent()->addCardInEnergyReserve($card);
            $element = $card->getElement()->value;

            $this->logService->addSendCardAsEnergyLog($element);
        }
    }

    private function failToDraw(): void
    {
        $this->logService->addEmptyDeckLog();
        $this->duel->setNoDrawAvailable(true);
    }

    /**
     * @param Card $card
     *
     * @throws \Exception
     */
    private function playCard(Card $card): void
    {
        // Play this card!
        $this->logService->addTryToPlayCardLog($card->getId(), $card->getCostsAsImages());

        switch ($card->getType()) {
            case CardType::Dinoz:
                $this->playDinozCard($card);
                break;
            case CardType::Spell:
                $this->playSpellCard($card);
                break;
            case CardType::Charm:
                $this->playCharmCard($card);
                break;
            default:
                // TODO Not sure if we should have both?!
                $errorMessage = sprintf("CardType (%s) Not implemented", $card->getType()->value);
                $this->logService->addErrorLog($errorMessage);
                throw new \Exception($errorMessage);
        }

        if ($card->hasKeyword(Keyword::Discharge)) {
            $this->duel->getCurrentOpponent()->removeEnergiesInReserve($card->getElement(), 1);
            $this->logService->addCapacityLog(Keyword::Discharge, [$card->getElement()->value], null);
        }
    }

    /**
     * Check if the card can be played within current context (charms / incantation can't be played if they don't have any valid target).
     * @param Card $card
     *
     * @return bool
     */
    private function checkIfATargetExists(Card $card): bool
    {
        $hasTarget = true;

        if ($card->getType() === CardType::Spell) {
            foreach ($card->getEffects() as $effect) {
                $targets = $this->effectInterpreter->computeEffectTargets($this->getDuel(), $effect);
                // Right now we only check the first effect, I don't know if it could cause a problem
                $hasTarget = count($targets) > 0;
                break;
            }
        } elseif ($card->getType() === CardType::Charm) {
            $card->instantiateCharm();
            $target = $this->effectInterpreter->computeCharmTarget($this->getDuel(), $card->getCharm());
            $hasTarget = $target !== null;
        }

        return $hasTarget;
    }

    /**
     * Try to play the Dinoz Card or discard it.
     * @param Card $card
     */
    private function playDinozCard(Card $card): void
    {
        $currentBoard = $this->duel->getCurrentOpponent()->getBoard();
        // Rule: We have remaining space, add the dinoz at the end of the Rail
        if ($currentBoard->canAddDinozOnBoard($card)) {
            $card->instantiateDinoz();
            if ($card->getDinoz()) {
                $this->duel->getCurrentOpponent()->getBoard()->addDinoz($card->getDinoz());
            }
        } else {
            // Rule : We don't have space, but we maybe have a coward dinoz which will replace the dinoz on the same place of the rail.
            $cowardDinozFilter = fn (Dinoz $dinoz) => !$dinoz->isEgg() && $dinoz->getOwningCard()->hasKeyword(Keyword::Coward);
            list($cowardDinoz, $cowardIndex) = $currentBoard->getOldestDinozFromList($currentBoard->filterDinoz($cowardDinozFilter));
            if (null !== $cowardDinoz) {
                $this->destroyCard($cowardDinoz->getOwningCard());
                $transferedHatchMarkerCount = $cowardDinoz->isEgg() ? $cowardDinoz->getHatchMarkerCount() : $cowardDinoz->getStat(DinocardStat::RequiredEclosion);
                $card->instantiateDinoz($transferedHatchMarkerCount);
                $currentBoard->replaceDinoz($card->getDinoz(), $cowardIndex);
                $this->logService->addCapacityLog(Keyword::Coward, [$cowardIndex + 1, $transferedHatchMarkerCount], null);
            } else {
                $this->logService->addNoPlaceOnBoardLog();
                $this->duel->getCurrentOpponent()->addCardInGraveyard($card);
            }
        }

        if ($newDinoz = $card->getDinoz()) {
            $this->logService->addDinozEnterBoardLog($newDinoz);
        }
    }

    /**
     * @param Card $card
     *
     * @throws \Exception
     */
    private function playSpellCard(Card $card): void
    {
        $this->logService->addCardLog($card->getId(), true, null);
        foreach ($card->getEffects() as $effectData) {
            $this->effectInterpreter->computeEffect($effectData);
        }
        $this->destroyCard($card);
    }

    /**
     * @param Card $card
     *
     * @throws \Exception
     */
    private function playCharmCard(Card $card): void
    {
        $card->instantiateCharm();
        if ($charm = $card->getCharm()) {
            // Note : we already compute the target in HasTarget, maybe we could have a single call to computeCharmTarget
            $charmTarget = $this->effectInterpreter->computeCharmTarget($this->getDuel(), $charm);

            if ($charmTarget instanceof Dinoz) {
                $charmTarget->addCharm($card->getCharm());
                $this->logService->addLog(sprintf("%s has been added to the dinoz %d", $charm, $charmTarget->getOwningCard()->getId()));
                $this->logService->addCardLog($card->getId(), true, null);

                // Spawn effects
                foreach ($card->getEffects() as $effect) {
                    $this->effectInterpreter->computeEffect($effect);
                }

                // Passive effects
                foreach ($charm->getEffects() as $effect) {
                    $this->effectInterpreter->computeEffect($effect);
                }
            }
        }
    }

    /**
     * Destroy the specified Charm
     * @param Charm $charm
     *
     * @return void
     */
    public function destroyCharm(Charm $charm): void
    {
        if (null !== $charm) {
            $affectedDinoz = $charm->getAffectedDinoz();
            $affectedDinoz->removeCharm($charm);

            // Compute all charm effect expiration
            // TODO Improve that
            foreach ($charm->getEffects() as $effect) {
                match ($effect->getActionType()) {
                    ActionType::TemporaryStat => $affectedDinoz->removeTemporaryStat("Card ".$charm->getOwningCard()->getId()),
                    ActionType::GainKeyword => $affectedDinoz->getOwningCard()->removeKeyword(Keyword::from($effect->getActionParameter("value"))),
                    default => throw new \Exception(sprintf("Charm effect expiration (%s) not implemented", $effect->getActionType()->name)),
                };
            }


            $this->destroyCard($charm->getOwningCard());
        }
    }

    /**
     * Destroy the specified card
     * @param Card          $card
     * @param Opponent|null $opponent
     *
     * @return void
     */
    private function destroyCard(Card $card, Opponent $opponent = null): void
    {
        if (null === $opponent) {
            $opponent = $this->duel->getCurrentOpponent();
        }

        if ($card->hasKeyword(Keyword::Wizard)) {
            $opponent->getEnergyReserve()->removeEnergySource($card, /*isIntangible*/true);
            $this->logService->addCapacityLog(Keyword::Wizard, [$card->getElement()->name], $opponent);
        }

        if ($card->hasKeyword(Keyword::Focus)) {
            $opponent->addCardInEnergyReserve($card);
            $this->logService->addCapacityLog(Keyword::Focus, [], $opponent);
        } else {
            $opponent->addCardInGraveyard($card);
        }
    }

    /**
     * End a Player Turn
     */
    private function endTurnPhase(): void
    {
        $this->logService->addTranslatedLog('FightLogs', 'phase.end');

        //TODO Handle this with the event system
        $this->disableBerserkBatch();

        $this->verifyDeadDinoz($this->getDuel()->getCurrentOpponent());
        $this->verifyDeadDinoz($this->getDuel()->getEnemyOpponent());
        $this->triggerHatch($this->duel->getCurrentOpponent());
        $this->triggerHatch($this->duel->getEnemyOpponent());
    }

    /**
     * Every dinoz with enough marker hatch.
     * @param Opponent $opponent
     */
    private function triggerHatch(Opponent $opponent): void
    {
        foreach ($opponent->getBoard()->getEggs() as $key => $egg) {
            $justHatched = $egg->verifyHatchMarker();
            if ($justHatched) {
                $this->logService->addHatchLog($key + 1, $egg->getOwningCard()->getId());

                if ($egg->getOwningCard()->hasKeyword(Keyword::Wizard)) {
                    $this->duel->getCurrentOpponent()->getEnergyReserve()->addEnergySource($egg->getOwningCard(), /*isIntangible*/ true);
                    $this->logService->addOldCapacityLog(Keyword::Wizard, sprintf("Player got one extra %s energy", $egg->getOwningCard()->getElement()->name));
                }
            }
        }
    }

    private function disableBerserkBatch(): void
    {
        /** @var Dinoz $dinoz */
        $filter = fn($dinoz) => $dinoz->hasTemporaryStatFromSource(Keyword::Berserk->value);
        $filteredDinozListA = $this->getDuel()->getFirstOpponent()->getBoard()->filterDinoz($filter);
        $filteredDinozListB = $this->getDuel()->getSecondOpponent()->getBoard()->filterDinoz($filter);
        $filteredDinozList = array_merge($filteredDinozListA, $filteredDinozListB);

        /** @var Dinoz $berserkDinoz */
        foreach ($filteredDinozList as $berserkDinoz) {
            $berserkDinoz->removeTemporaryStat(Keyword::Berserk->value);
            $this->logService->addOldCapacityLog(Keyword::Berserk, sprintf(
                "Dinoz (%d) rage expired ! New Stats : %d/%d",
                $berserkDinoz->getOwningCard()->getId(),
                $berserkDinoz->getStat(DinocardStat::Strength),
                $berserkDinoz->getHealth()
            ));
        }
    }

    /**
     * Each dinoz attacks what is in front of them THEN we compute who died.
     */
    private function attackPhase(): void
    {
        $this->logService->addAttackPhaseLogs();

        /** @var Dinoz $dinoz */
        $initiativeDinozFilter = fn($dinoz) => $dinoz->canAttack() && $dinoz->getOwningCard()->hasKeyword(Keyword::Initiative);
        $this->triggerAttackBatch($initiativeDinozFilter, /*shiftDinoz*/false, /*debugName*/ "Initiative");

        /** @var Dinoz $dinoz */
        $remainingDinozFilter = fn($dinoz) => $dinoz->canAttack() && false === $dinoz->getOwningCard()->hasKeyword(Keyword::Initiative);
        $this->triggerAttackBatch($remainingDinozFilter, /*shiftDinoz*/true, /*debugName*/ "Regular");
    }

    /**
     * Try to trigger a batch of attack by filtering the dinoz that can attack.
     * Returns true if any attacks occurred.
     * @param callable $filter
     * @param bool     $shiftDinoz
     * @param string   $debugName
     *
     * @return bool
     */
    private function triggerAttackBatch(callable $filter, bool $shiftDinoz, string $debugName): void
    {
        $opponentA = $this->getDuel()->getFirstOpponent();
        $opponentB = $this->getDuel()->getSecondOpponent();
        $filteredDinozListA = $opponentA->getBoard()->filterDinoz($filter);
        $filteredDinozListB = $opponentB->getBoard()->filterDinoz($filter);
        $isAnyAttack = count($filteredDinozListA) > 0 || count($filteredDinozListB) > 0;

        if ($isAnyAttack) {
            $this->logService->addLog(sprintf("Attacks (<b>%s</b>)", $debugName));
        }
        if (count($filteredDinozListA) > 0) {
            $this->triggerAttacks($opponentA, $filteredDinozListA, $opponentB);
        }
        if (count($filteredDinozListB) > 0) {
            $this->triggerAttacks($opponentB, $filteredDinozListB, $opponentA);
        }
        if ($isAnyAttack) {
            $this->verifyDeadDinoz($opponentA, $shiftDinoz);
            $this->verifyDeadDinoz($opponentB, $shiftDinoz);
        }

        $this->logService->addBlankLog();
    }

    /**
     * @param Opponent $attacker
     * @param array    $attackerDinozList Which dinoz are attacking?
     * @param Opponent $defender
     */
    private function triggerAttacks(Opponent $attacker, array $attackerDinozList, Opponent $defender): void
    {
        $defenderDinozList = $defender->getBoard()->getDinoz();

        /**
         * @var int $boardIndex
         * @var Dinoz $attackingDinoz
         */
        foreach ($attackerDinozList as $boardIndex => $attackingDinoz) {
            if ($attackingDinoz->canAttack()) {
                $dinozStrength = $attackingDinoz->getStat(DinocardStat::Strength);
                $damageOnPlayer = $dinozStrength;

                list($dinozInDefenseIndex, $defenseMotive) = $this->getDinozInDefenseIndex($attackingDinoz, $boardIndex, $defenderDinozList);
                if (null !== $dinozInDefenseIndex) {
                    $dinozInDefense = $defenderDinozList[$dinozInDefenseIndex];
                    if ($attackingDinoz->canAttackDinoz($dinozInDefense)) {
                        if ($attackingDinoz->getOwningCard()->hasKeyword(Keyword::Berserk)) {
                            $berserkCount = $attackingDinoz->getOwningCard()->countKeyword(Keyword::Berserk);
                            $attackingDinoz->addTemporaryStat(Keyword::Berserk->value, [0, $berserkCount, -$berserkCount, 0]);
                            $this->logService->addOldCapacityLog(Keyword::Berserk, sprintf(
                                "%dth Dinoz enters in rage ! new Stats : %d/%d",
                                $boardIndex+1,
                                $attackingDinoz->getStat(DinocardStat::Strength),
                                $attackingDinoz->getHealth()
                            ));
                            $dinozStrength = $attackingDinoz->getStat(DinocardStat::Strength);
                        }
                        
                        list($appliedDamage, $remainingDamage) = $dinozInDefense->applyDamage($dinozStrength);
                        
                        if (null !== $defenseMotive && $boardIndex !== $dinozInDefenseIndex) {
                            $this->logService->addOldCapacityLog($defenseMotive, sprintf("Target has changed!"));
                        }
                        $this->logService->addDinozAttackLog($dinozInDefense, $boardIndex, $dinozInDefenseIndex, $appliedDamage, $attacker);

                        if ($attackingDinoz->getOwningCard()->hasKeyword(Keyword::Tenacious) && $dinozInDefense->isDead() && $remainingDamage > 0) {
                            $damageOnPlayer = $remainingDamage;
                            $this->logService->addCapacityLog(Keyword::Tenacious, [$boardIndex + 1, $damageOnPlayer]);
                        } else {
                            $damageOnPlayer = 0;
                        }
                        if ($attackingDinoz->getOwningCard()->hasKeyword(Keyword::Venomous) && !$dinozInDefense->isEgg()) {
                            $dinozInDefense->getOwningCard()->addKeyword(Keyword::Poisoned);
                            $this->logService->addCapacityLog(Keyword::Venomous, [$boardIndex + 1, $dinozInDefenseIndex + 1]);
                        }
                        if ($attackingDinoz->getOwningCard()->hasKeyword(Keyword::Assassin) && $appliedDamage > 0) {
                            $dinozInDefense->setForcedDeath(true);
                            $this->logService->addCapacityLog(Keyword::Assassin, [$boardIndex + 1, $dinozInDefenseIndex + 1]);
                        }
                    }
                }

                if ($damageOnPlayer > 0) {
                    $defender->modifyHealth(-$damageOnPlayer);
                    $this->logService->addDinozAttackedPlayerLog($boardIndex, $damageOnPlayer, $attacker);
                }
            }
        }
    }

    /**
     * Rules by priority
     * Attacker has Rock        : Rock are blocking attacks from any dinoz (even the one flying)
     * Attacker has Pack        : attacker will always target the dinoz on the left of the board
     * Defender has Survivor    : attack will be redirected to the oldest non survivor if any
     * Default Rule             : we try to attack the dinoz in front of us.
     *
     * @param Dinoz   $attacker
     * @param int     $boardIndexAttacker
     * @param Dinoz[] $defenderDinozList
     *
     * @return array|null
     */
    private function getDinozInDefenseIndex(Dinoz $attacker, int $boardIndexAttacker, array $defenderDinozList): ?array
    {
        $returnIndex = null;
        $returnMotive = null;
        if (count($defenderDinozList) > 0) {
            $oldestRockDefenderAge = -1;
            $oldestRockDefenderIndex = null;
            foreach ($defenderDinozList as $index => $dinoz) {
                if (!$dinoz->isEgg() && $dinoz->getOwningCard()->hasKeyword(Keyword::Rock)) {
                    if ($dinoz->getStat(DinocardStat::Age) > $oldestRockDefenderAge) {
                        $oldestRockDefenderIndex = $index;
                        $oldestRockDefenderAge = $dinoz->getStat(DinocardStat::Age);
                    }
                }
            }

            if (null !== $oldestRockDefenderIndex) {
                $returnIndex = $oldestRockDefenderIndex;
                $returnMotive = Keyword::Rock;
            } else {
                if ($attacker->getOwningCard()->hasKeyword(Keyword::Pack)) {
                    //Get First Dinoz, or First index if the attacker has Stompy
                    if (array_key_exists(0, $defenderDinozList) && $attacker->getOwningCard()->hasKeyword(Keyword::Stompy)) {
                        $returnIndex = 0;
                    } else {
                        $hatchedDinozFilter = fn($dinoz) => !$dinoz->isEgg();
                        $hatchedDinoz = array_filter($defenderDinozList, $hatchedDinozFilter);
                        $returnIndex = array_key_first($hatchedDinoz);
                    }
                    $returnMotive = Keyword::Pack;
                } else {
                    if (array_key_exists($boardIndexAttacker, $defenderDinozList)) {
                        $returnIndex = $boardIndexAttacker;
                    }
                }
            }

            if (null !== $returnIndex) {
                if ($defenderDinozList[$returnIndex]->getOwningCard()->hasKeyword(Keyword::Survivor)) {
                    //Get Oldest non Survivor Defender
                    $nonSurvivorfilter = fn($dinoz) => $dinoz->canAttack() && !$dinoz->getOwningCard()->hasKeyword(Keyword::Survivor);
                    $nonSurvivorDinoz = array_filter($defenderDinozList, $nonSurvivorfilter);
                    if (count($nonSurvivorDinoz) > 0) {
                        uasort($nonSurvivorDinoz, function ($first, $second) {
                            return $first->getStat(DinocardStat::Age) > $second->getStat(DinocardStat::Age);
                        });
                        $returnIndex = array_key_first($nonSurvivorDinoz);
                        $returnMotive = Keyword::Survivor;
                    }
                }
            }
        }

        return array($returnIndex, $returnMotive);
    }

    /**
     * Verify if any dinoz on the board is dead.
     * Optionally shift all the dinoz as left as possible.
     * @param Opponent $opponent
     * @param bool     $shiftDinoz
     */
    private function verifyDeadDinoz(Opponent $opponent, bool $shiftDinoz = true): void
    {
        foreach ($opponent->getBoard()->getDinoz() as $dinozIndex => $dinoz) {
            if ($dinoz->isDead()) {
                $opponent->getBoard()->removeDinoz($dinozIndex);
                $this->logService->addDinozDiedLog($dinozIndex, $opponent);
                $this->destroyCard($dinoz->getOwningCard(), $opponent);

                try {
                    foreach ($dinoz->getCharms() as $charm) {
                        $this->destroyCharm($charm);
                    }
                } catch (\Exception $e) {
                    $this->logService->addErrorLog("[Charm destruction] ".$e->getMessage());
                }
            }
        }
        if ($shiftDinoz) {
            $opponent->getBoard()->shiftDinozOnBoard();
        }
    }

    public function setSeed(int $seed)
    {
        $this->randomSeed = $seed;
        srand($seed);
    }

    public function getSeed(): int
    {
        return $this->randomSeed;
    }

    public function randomizeSeed()
    {
        $this->randomSeed = rand();
        srand($this->randomSeed);
    }

    /**
     * @return string
     */
    public function getFightUrl(): string
    {
        return $this->fightUrl;
    }

    /**
     * @param string $fightUrl
     */
    public function setFightUrl(string $fightUrl): void
    {
        $this->fightUrl = $fightUrl;
    }
}