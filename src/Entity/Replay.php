<?php

namespace App\Entity;

use App\Repository\ReplayRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReplayRepository::class)
 * @ORM\Table(name="replay")
 */
class Replay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * Many replays have the same Opponents.
     * @ORM\ManyToOne(targetEntity="User")
     */
    private User $firstOpponent;

    /**
     * Many replays have the same Opponents.
     * @ORM\ManyToOne(targetEntity="User")
     */
    private User $secondOpponent;

    /**
     * Many replays have the same Decks.
     * @ORM\ManyToOne(targetEntity="DeckDB")
     */
    private DeckDB $firstDeck;

    /**
     * Many replays have the same Decks.
     * @ORM\ManyToOne(targetEntity="DeckDB")
     */
    private DeckDB $secondDeck;

    /**
     * @ORM\Column(type="integer")
     */
    private int $seed = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $won = false;

    /**
     * @param DeckDB $deckA
     * @param DeckDB $deckB
     * @param int    $seed
     * @param bool   $won
     */
    public function __construct(DeckDB $deckA, DeckDB $deckB, int $seed, bool $won)
    {
        $userA = $deckA->getOwner();
        $userB = $deckB->getOwner();
        $this->createdAt = new \DateTime();
        $this->firstOpponent = $userA;
        $this->secondOpponent = $userB;

        $this->firstDeck = $deckA;
        $this->secondDeck = $deckB;
        $this->seed = $seed;
        $this->won = $won;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getFirstOpponent(): User
    {
        return $this->firstOpponent;
    }

    /**
     * @return User
     */
    public function getSecondOpponent(): User
    {
        return $this->secondOpponent;
    }

    /**
     * @return DeckDB
     */
    public function getFirstDeck(): DeckDB
    {
        return $this->firstDeck;
    }

    /**
     * @return DeckDB
     */
    public function getSecondDeck(): DeckDB
    {
        return $this->secondDeck;
    }

    /**
     * @return array<User>
     */
    public function getOpponents(): array
    {
        return [$this->firstOpponent, $this->secondOpponent];
    }


    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getSeed(): int
    {
        return $this->seed;
    }

    /**
     * @return bool
     */
    public function getWon(): bool
    {
        return $this->won;
    }
}