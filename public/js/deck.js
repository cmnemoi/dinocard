/* coding: utf-8 */

// TODO See if we can use twig + js to handle translation

//#region Deck creation/edition

let isDeckModificationLocked = false;

/**
 * Add one instance of the card to the deck list
 * @param cardId
 * @param sameMax How much card of the same ID can we have?
 */
function addOneCard(cardId, sameMax)
{
    if (isDeckModificationLocked) {
        return;
    }

    // Check if we have this card in our collection
    let amountCollectionDiv = document.getElementById("nb_" + cardId);
    let amountInCollection = amountCollectionDiv ? parseInt(amountCollectionDiv.innerHTML) - 1 : 0;
    if (amountInCollection < 0) {
        return;
    }

    // Check if we already have the card in the list?
    let cardLabel = getCardAmountLabel(cardId);
    let isCardInTheList = cardLabel != null;
    let newAmount = 1 + parseCardAmount(cardLabel);

    if (newAmount > sameMax) {
        alert("Impossible d'ajouter plus de " + sameMax + " exemplaires de la carte " + cardId);

        return;
    }

    let totalCardDiv = document.getElementById("nbCards");
    let totalCardNb = parseInt(totalCardDiv.innerHTML);
    if (totalCardNb + 1 >= 100) {
        alert("Impossible d'ajouter davantage de cartes : un pack ne peut pas contenir plus de 100 cartes.");

        return;
    }

    let viewDiv = document.getElementById("view");
    if (viewDiv === null) {
        return;
    }
    isDeckModificationLocked = true;

    totalCardDiv.innerHTML = (totalCardNb + 1).toString();

    let cardType = document.getElementById("type_"+cardId).innerHTML;
    setDeckStats(cardId, true, cardType);

    if (cardLabel !== null) {
        refreshCardAmount(cardLabel, newAmount);
    }

    if (!isCardInTheList) {
        let cardListDiv = document.getElementById("listCards");
        let cardList = cardListDiv.querySelector("ul");

        // TODO Store the "Empty list element" to restore it later. We don't want to store translation in the JS!
        if (totalCardNb === 0) {
            cardList.innerHTML = "";
        }

        cardList.appendChild(createCardListItem(cardId, newAmount));
    }

    // Update our collection
    amountCollectionDiv.innerHTML = amountInCollection.toString();
    if (amountInCollection > 0) {
        let amountFeedback = document.getElementById("ex_" + cardId);
        if (amountFeedback !== null) {
            if (amountInCollection > 1) {
                amountFeedback.innerHTML = "<img class=\"icon\" src=\"/img/gfx/font/x.gif\" alt=\"xxx\" />" + numberToImage("/img/gfx/font/", amountInCollection);
            } else {
                amountFeedback.innerHTML = "";
            }
        }
    } else {
        let card = document.getElementById("card" + cardId);
        card.style.display = "none";
    }

    isDeckModificationLocked = false;
}

/**
 * Remove one instance of the card from the deck list
 * @param evt HtmlElement clicked
 */
function removeOneCard(evt)
{
    if (isDeckModificationLocked) {
        return;
    }

    let cardId = evt.currentTarget.cardId;

    let cardLabel = getCardAmountLabel(cardId);
    let cardNb = parseCardAmount(cardLabel);
    if (cardNb < 1) {
        return;
    }

    isDeckModificationLocked = true;

    let totalCardDiv = document.getElementById("nbCards");
    totalCardDiv.innerHTML = (parseInt(totalCardDiv.innerHTML) - 1).toString();

    let cardType = document.getElementById("type_" + cardId).innerHTML;
    setDeckStats(cardId, false, cardType);

    // Update list of cards
    let listCardsDiv = document.getElementById("listCards");
    cardNb -= 1;
    refreshCardAmount(cardLabel, cardNb);
    if (cardNb === 0) {
        removeCardListItem(listCardsDiv, cardId);
    }

    if (listCardsDiv.childElementCount === 0) {
        listCardsDiv.innerHTML = "<ul><li><i>Aucune carte ajoutée.</i></li></ul>";
    }

    // Update Pool of available cards
    let amountCollectionDiv = document.getElementById("nb_" + cardId);
    if (amountCollectionDiv !== null) {
        let amountInCollection = parseInt(amountCollectionDiv.innerHTML) + 1;
        amountCollectionDiv.innerHTML = amountInCollection.toString();

        if (amountInCollection > 1) {
            let amountFeedback = document.getElementById("ex_" + cardId);
            if (amountFeedback !== null) {
                amountFeedback.innerHTML = "<img class=\"icon\" src=\"/img/gfx/font/x.gif\" alt=\"xxx\" />" + numberToImage("/img/gfx/font/", amountInCollection);
            }
        }
        if (amountInCollection === 1) {
            //on vire le display:none de la carte
            let c = document.getElementById("card" + cardId);
            c.style.display = "inline";
        }
    }

    isDeckModificationLocked = false;
}

/**
 * Create the List Item for the specified card
 * @param cardId
 * @param amount
 * @returns {HTMLLIElement}
 */
function createCardListItem(cardId, amount)
{
    let name = document.getElementById("name" + cardId).innerHTML;
    let desc = document.getElementById("desc" + cardId).innerHTML;

    let cache = document.getElementById("cache_" + cardId);
    let cardType = cache.querySelector("#type_" + cardId).innerHTML;
    let elem = cache.querySelector("#element_" + cardId).innerHTML;
    let energy = cache.querySelector("#energy_" + cardId).innerHTML;

    let dinozStats = "";
    if (cardType === "dinoz") {
        let atk = cache.querySelector("#atk_" + cardId).innerHTML;
        let def = cache.querySelector("#def_" + cardId).innerHTML;
        let ecl = cache.querySelector("#ecl_" + cardId).innerHTML;
        dinozStats = " - " + ecl + " / " + atk + " / " + def;
    }

    let listItem = document.createElement('li');
    listItem.cardId = cardId;
    listItem.id = "li_" + cardId;

    let removeLink = document.createElement('a');
    removeLink.id = "rm_" + cardId;
    removeLink.addEventListener("click", removeOneCard, false);
    removeLink.innerHTML = "<img class='icon' src='/img/gfx/icons/minus.gif' alt='minus symbol'/>";
    removeLink.cardId = cardId;
    removeLink.className = "clickable";
    listItem.appendChild(removeLink);

    let amountLabel = document.createElement("label");
    amountLabel.id = "nbT_" + cardId;
    amountLabel.amount = amount;
    amountLabel.innerHTML = " " + amount + " x";
    listItem.appendChild(amountLabel);

    let cardLink = document.createElement('a');
    cardLink.className = "cardNameLink" + elem;
    cardLink.target = "popup";
    cardLink.href = "/card/view/" + cardId;
    cardLink.addEventListener("mouseover", showCardInfoTooltip);
    cardLink.addEventListener("mouseout", hideTip);
    cardLink.tooltipTarget = cardLink;
    cardLink.tooltipTitle = getCardInfoAsImages(elem, cardType) + "<span>&nbsp;" + getCardCostAsImages(energy) + "</span>";
    cardLink.tooltipContent = "<b>" + cardType + dinozStats + "</b><br />" + desc;
    cardLink.innerHTML = name;
    listItem.appendChild(cardLink);

    return listItem;
}

/**
 * Delete the List item of the specified card
 * @param cardsList
 * @param cardId
 */
function removeCardListItem(cardsList, cardId)
{
    let list = cardsList.querySelector("ul");
    let listItem = document.getElementById("li_"+cardId);
    list.removeChild(listItem);
}

/**
 * OnMouseOver Callback that shows a Card tooltip
 * @param evt (should contains the tooltip info)
 */
function showCardInfoTooltip(evt)
{
    showTip(evt.target.tooltipTarget, evt.target.tooltipTitle, evt.target.tooltipContent);
}

/**
 * Update the total amount of cards and the counter for each type
 */
function setDeckStats(cardId, add, type)
{
    let nb = document.getElementById(type + "Nb");
    if (nb === null) {
        console.error("Can't find the Stat dom element. Please debug!"); return;
    }
    if (add) {
        nb.innerHTML = (parseInt(nb.innerHTML) + 1).toString();
    } else {
        nb.innerHTML = (parseInt(nb.innerHTML) - 1).toString();
    }
}

/**
 * Get the amount of the specified label (associated to a card)
 * @param cardLabel
 * @returns {number}
 */
function parseCardAmount(cardLabel)
{
    return parseInt(cardLabel ? cardLabel.amount : 0);
}

/**
 * Refresh the amount displayed of the specified card.
 * @param cardNbLabel
 * @param amount
 * @return void
 */
function refreshCardAmount(cardNbLabel, amount)
{
    cardNbLabel.amount = amount;
    cardNbLabel.innerHTML = " " + amount + " x";
}

/**
 * Retrieve the label containing the amount of the specified card
 * @param cardId
 * @returns {HTMLElement}
 */
function getCardAmountLabel(cardId)
{
    return document.getElementById("nbT_" + cardId);
}

/**
 * Generates a list of img to feedback the card costs.
 * @return string
 */
function getCardCostAsImages(cardCost)
{
    let costArray = cardCost.split('/');
    let res = "";
    // Note : last element should be empty!
    for (let i = 0; i < costArray.length - 1; i++) {
        let element = costArray[i];
        res = "<img alt='"+element+"' src='" + "/img/gfx/icons/energy_"+element+".gif' />&nbsp;" + res;
    }

    return res;
}

/**
 * Generates a list of images to feedback the card element and type.
 * @return string
 */
function getCardInfoAsImages(elem, cardType)
{
    let res = "<img class='icon' src='/img/gfx/icons/element_" + elem + ".gif' alt='icon " + elem + "'/>";
    res += "&nbsp";
    res += "<img class='icon' src='/img/gfx/icons/icon_" + cardType + ".gif' alt='icon " + cardType + "'/>";

    return res;
}

/**
 * On player clicked on Save.
 * Prepare the list of cards before and send the request to the server
 */
function saveDeck()
{
    let nbCards = document.getElementById("nbCards");
    if (nbCards.innerHTML < 20) {
        return;
    }

    let listCardsDiv = document.getElementById("listCards");
    let cardsList = listCardsDiv.querySelector("ul").children;

    let deckData = "";
    for (let i = 0; i < cardsList.length; i++) {
        let child = cardsList[i];

        let cardId = child.cardId;
        let cardLabel = getCardAmountLabel(cardId);
        let amount = parseCardAmount(cardLabel);

        deckData += cardId + "#" + amount;
        if (i + 1 < cardsList.length) {
            deckData += " ";
        }
    }

    // Note: pc means "pack cards"
    document.getElementById('pc').value = deckData;
    requestSave('fNewPack');
}

//#endregion

//#region Server requests

/**
 *  the DeckController/delete
 * @param formId
 * @param packId
 * @return void
 */
function requestDelete(formId, packId)
{
    if (confirm("Voulez-vous vraiment dissoudre ce pack ?")) {
        let form = document.getElementById(formId);
        form.action = "/deck/delete/" + packId;
        form.submit();
    }
}

/**
 * Either confirm the deck creation or edition
 * @param formId
 * @return void
 */
function requestSave(formId)
{
    let hid = document.getElementById("name");
    let form = document.getElementById(formId);
    let nbCards = document.getElementById("nbCards");
    if (nbCards.innerHTML < 20) {
        alert("Le nouveau pack ne contient que " + nbCards.innerHTML + " carte(s). Un pack doit en contenir au minimum 20.");
    } else {
        if (hid.value === "new") {
            let name = document.getElementById("packSetName").value;
            if ((name === null) || (name === "")) {
                alert("nouveau deck non sauvegardé : aucun nom saisi. ");
            } else {
                hid.value = name;
                form.submit();
            }
        } else {
            form.submit();
        }
    }
}

//#endregion