<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\Keyword;

class KeywordFilter extends TargetFilter
{
    private bool $isInverted;
    private Keyword $searchedKeyword;

    /**
     * @param Card  $card
     * @param array $input
     */
    public function __construct(Keyword $searchedKeyword, bool $inverted = false)
    {
        $this->searchedKeyword = $searchedKeyword;
        $this->isInverted = $inverted;
    }

    public function filter($targets){
        if ($this->isInverted) {
            $filter = fn($dinoz) => !$dinoz->getOwningCard()->hasKeyword($this->searchedKeyword);
        } else {
            $filter = fn($dinoz) => $dinoz->getOwningCard()->hasKeyword($this->searchedKeyword);
        }
        return array_filter($targets, $filter);
    }
}