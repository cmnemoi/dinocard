<?php

namespace App\Challenge\Entity;

use App\Challenge\Entity\EnergySystem\IEnergySource;
use App\Challenge\Enum\CardType;
use App\Challenge\Enum\ElementType;
use App\Challenge\Enum\Keyword;
use App\Challenge\Enum\RarityType;
use App\Service\Utils;

class Card implements IEnergySource
{
    /** @var int Unique identifier */
    private int $id;

    private CardType $type;
    private RarityType $rarity;

    private ElementType $element;

    /** @var array<CardCost> */
    private array $costs = [];

    // TODO Use it as a dictionnary<Keyword, count> for more efficiency
    /** @var array<Keyword>  */
    private array $keywords = [];

    /**
     * @var array<Effect> Each element of the array contains the json description of an effect
     */
    private array $effects = [];

    /** @var array Json that contains specific card data */
    private array $extraData;

    private ?Dinoz $dinoz = null;
    private ?Charm $charm = null;

    /**
     * @param int         $id
     * @param CardType    $cardType
     * @param ElementType $mainElement
     * @param RarityType  $rarityType
     */
    public function __construct(int $id, CardType $cardType, ElementType $mainElement, RarityType $rarityType)
    {
        $this->id = $id;
        $this->type = $cardType;
        $this->rarity = $rarityType;
        $this->element = $mainElement;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return CardType
     */
    public function getType(): CardType
    {
        return $this->type;
    }

    /**
     * @return RarityType
     */
    public function getRarity(): RarityType
    {
        return $this->rarity;
    }

    /**
     * @return ElementType
     */
    public function getElement(): ElementType
    {
        return $this->element;
    }

    /**
     * @return CardCost[]
     */
    public function getCosts(): array
    {
        return $this->costs;
    }

    /**
     * Note: This is used in TWIG to easily read costs in javascript
     * @return string
     */
    public function getCostsToString(): string
    {
        $costString = "";
        foreach ($this->costs as $cost) {
            $costString .= str_repeat($cost->getElement()->value.'/', $cost->getCount());
        }

        return $costString;
    }

    /**
     * @return string
     */
    public function getCostsAsImages(): string
    {
        $costsAsImagesString = "";
        foreach ($this->costs as $cost) {
            $costsAsImagesString .= sprintf('[%s %d]', Utils::getEnergyAsImage($cost->getElement()->value), $cost->getCount());
        }

        return $costsAsImagesString;
    }

    /**
     * @param CardCost $cost
     */
    public function addCost(CardCost $cost)
    {
        $this->costs[] = $cost;
    }

    /**
     * Returns the energy cost for the specified energy
     * @param ElementType $element
     *
     * @return int
     */
    public function getEnergyCost(ElementType $element): int
    {
        $cost = 0;
        foreach ($this->costs as $cardCost) {
            if ($element === $cardCost->getElement()) {
                $cost = $cardCost->getCount();
                break;
            }
        }
        return $cost;
    }

    /**
     * Count the energy cost for all energies types
     *
     * @return int
     */
    public function countCost(): int
    {
        $cost = 0;
        foreach (ElementType::cases() as $element) {
            $cost += $this->getEnergyCost($element);
        }
        return $cost;
    }

    /**
     * @return string[]
     */
    public function getKeywords(): array
    {
        return $this->keywords;
    }

    /**
     * @param Keyword $keyword
     */
    public function addKeyword(Keyword $keyword)
    {
        $this->keywords[] = $keyword;
    }

    /**
     * @param Keyword $keyword
     */
    public function removeKeyword(Keyword $keyword)
    {
        while (($key = array_search($keyword, $this->keywords, true)) !== false) {
            unset($this->keywords[$key]);
        }
    }

    /**
     * @param Keyword $keywordChecked
     *
     * @return bool
     */
    public function hasKeyword(Keyword $keywordChecked): bool
    {
        return in_array($keywordChecked, $this->keywords);
    }

    /**
     * @param Keyword $keywordChecked
     *
     * @return int
     */
    public function countKeyword(Keyword $keywordChecked): int
    {
        $count = 0;
        foreach ($this->keywords as $keyword) {
            if ($keyword === $keywordChecked) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @param int $initialHatchMarker
     */
    public function instantiateDinoz(int $initialHatchMarker = 0) : void
    {
        if (CardType::Dinoz === $this->getType()) {
            if (null === $this->dinoz) {
                if (count($this->extraData) !== 0) {
                    $this->dinoz = new Dinoz($this);
                    $this->dinoz->setHatchMarker($initialHatchMarker);
                }
            }
        }
    }

    /**
     * @return void
     */
    public function instantiateCharm() : void
    {
        if (CardType::Charm === $this->getType()) {
            if (null === $this->charm) {
                if (count($this->extraData) !== 0) {
                    $this->charm = new Charm($this);
                }
            }
        }
    }

    /**
     * @return Dinoz|null
     */
    public function getDinoz() : ?Dinoz
    {
        return $this->dinoz;
    }

    /**
     * @return Charm|null
     */
    public function getCharm() : ?Charm
    {
        return $this->charm;
    }

    /**
     * @return array
     */
    public function getExtraData(): array
    {
        return $this->extraData;
    }

    /**
     * @param array $extraData
     *
     * @return void
     */
    public function setExtraData(array $extraData)
    {
        $this->extraData = $extraData;
    }

    /**
     * IEnergySource implementation
     * @return ElementType
     */
    public function getEnergyElement(): ElementType
    {
        return $this->element;
    }

    /**
     * @return array
     */
    public function getEffects(): array
    {
        return $this->effects;
    }

    /**
     * @param array $effectData Effect Json Data
     *
     * @return void
     */
    public function addEffect(array $effectData)
    {
        $this->effects[] = new Effect($this, $effectData);
    }
}
