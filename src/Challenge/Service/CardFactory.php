<?php

namespace App\Challenge\Service;

use App\Challenge\Entity\Card;
use App\Challenge\Entity\CardCost;
use App\Challenge\Enum\CardType;
use App\Challenge\Enum\ElementType;
use App\Challenge\Enum\Keyword;
use App\Challenge\Enum\RarityType;
use App\Service\JsonLoader;

class CardFactory
{
    private array $cardsData;

    /**
     * @param JsonLoader $jsonLoader
     */
    public function __construct(JsonLoader $jsonLoader)
    {
        // Is that expensive?
        $this->cardsData = $jsonLoader->loadJsonFromPublicFolder("cardData");
    }

    /**
     * Return all ID described in the cardData json file.
     * @return array<int>
     */
    public function getAllCardId(): array
    {
        return array_keys($this->cardsData);
    }

    /**
     * @param int $id
     *
     * @return Card|null
     */
    public function loadCard(int $id) : ?Card
    {
        $card = null;
        if (array_key_exists($id, $this->cardsData)) {
            $cardData = $this->cardsData[$id];

            $rarity = RarityType::Common;
            if (array_key_exists("rarity", $cardData)) {
                $rarity = RarityType::tryFrom($cardData["rarity"]);
            }

            $card = new Card($id, CardType::tryFrom($cardData["type"]), ElementType::tryFrom($cardData["element"]), $rarity);
            foreach ($cardData['costs'] as $cost) {
                $card->addCost(new CardCost(ElementType::from($cost["element"]), $cost["amount"]));
            }

            if (array_key_exists("typeData", $cardData)) {
                $card->setExtraData($cardData["typeData"]);
            }

            if (array_key_exists("keywords", $cardData)) {
                foreach ($cardData['keywords'] as $keyword) {
                    $card->addKeyword(Keyword::from($keyword));
                }
            }

            if (array_key_exists("effects", $cardData)) {
                foreach ($cardData["effects"] as $effectData) {
                    $card->addEffect($effectData);
                }
            }
        }

        return $card;
    }
}
