<?php

namespace App\Security;

use App\Service\OAuthService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

/**
 * Symfony authenticator: https://symfony.com/doc/current/security/guard_authentication.html#step-2-create-the-authenticator-class
 */
class OAuthTokenAuthenticator extends AbstractGuardAuthenticator
{
    /** @var OAuthService  */
    private $oauthService;
    /** @var RouterInterface  */
    private $router;

    public function __construct(OAuthService $oauthService, RouterInterface $router)
    {
        $this->oauthService = $oauthService;
        $this->router = $router;
    }

    public function supports(Request $request) : bool
    {
        return $request->get('_route') === 'oauth_callback' && !empty($request->get('code'));
    }

    public function getCredentials(Request $request)
    {
        return $request->get('code');
    }

    public function getUser($credentials, UserProviderInterface $userProvider) : ?UserInterface
    {
        if (null === $credentials) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            return null;
        }

        return $this->oauthService->login($credentials);
    }

    public function checkCredentials($credentials, UserInterface $user) : bool
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) : ?Response
    {
        return new Response('login error', Response::HTTP_UNAUTHORIZED);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey) : ?Response
    {
        $redirect = $request->get('state');

        if (!$redirect) {
            $redirect = $this->router->generate('homepage');
        }

        return new RedirectResponse($redirect);
    }

    public function start(Request $request, AuthenticationException $authException = null) : ?Response
    {
        return new RedirectResponse($this->oauthService->getAuthorizationUri('base', $request->getRequestUri()));
    }

    public function supportsRememberMe() : bool
    {
        return false;
    }
}