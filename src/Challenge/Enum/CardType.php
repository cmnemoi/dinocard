<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum CardType: string
{
    case Dinoz = "dinoz";
    case Spell = "spell";
    case Item = "item";
    case Charm = "charm";
}