<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum RarityType: string
{
    case Common = "common";
    case Uncommon = "uncommon";
    case Rare = "rare";
}