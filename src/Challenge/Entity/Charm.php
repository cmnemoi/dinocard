<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\DinocardStat;
use App\Challenge\Enum\Keyword;
use App\Challenge\Enum\TargetEntity;
use App\Challenge\Enum\TargetSide;

class Charm
{
    // Do we need an interface for that?
    private Card $owningCard;

    private Dinoz $affectedDinoz;

    /** @var array<Effect> $effects */
    private array $effects;

    /** @var array<TargetSide> */
    private array $targetSide;

    /** @var array<TargetEntity> */
    private array $targetEntity;
    
    /** @var array<TargetFilter> */
    private array $targetFilter;

    /**
     * @param Card $card
     */
    public function __construct(Card $card)
    {
        $this->owningCard = $card;

        $jsonData = $card->getExtraData();
        $this->targetSide = TargetSide::parse($jsonData["charmTarget"]["side"]);
        $this->targetEntity = TargetEntity::parse($jsonData["charmTarget"]["entity"]);
        $this->targetFilter = array();
        
        if (array_key_exists("filter", $jsonData["charmTarget"])) {
            $this->targetFilter = array();
            foreach($jsonData["charmTarget"]["filter"] as $filterJson){
                array_push($this->targetFilter, TargetFilter::initialize($filterJson));
            }
        }

        $passiveEffects = $jsonData["passiveEffects"];
        foreach ($passiveEffects as $effectData) {
            $this->addEffect($effectData);
        }
    }

    /**
     * @return Card
     */
    public function getOwningCard(): Card
    {
        return $this->owningCard;
    }
    
    /**
     * @return array
     */
    public function getTargetSide(): array
    {
        return $this->targetSide;
    }

    /**
     * @return array
     */
    public function getTargetEntity(): array
    {
        return $this->targetEntity;
    }

    /**
     * @return array
     */
    public function getTargetFilter(): array
    {
        return $this->targetFilter;
    }

    /**
     * @return Dinoz
     */
    public function getAffectedDinoz(): Dinoz
    {
        return $this->affectedDinoz;
    }

    /**
     * @param Dinoz $affectedDinoz
     */
    public function setAffectedDinoz(Dinoz $affectedDinoz): void
    {
        $this->affectedDinoz = $affectedDinoz;
    }

    /**
     * @return array
     */
    public function getEffects(): array
    {
        return $this->effects;
    }

    /**
     * @param array $effectData Effect Json Data
     *
     * @return void
     */
    public function addEffect(array $effectData)
    {
        $this->effects[] = new Effect($this->getOwningCard(), $effectData);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "Charm ".$this->getOwningCard()->getId();
    }
}
