<?php

namespace App\Controller;

use App\Challenge\Enum\DeckPreset;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    private ManagerRegistry $managerRegistry;

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @Route("/admin", name="admin")
     *
     * @return Response
     */
    public function adminIndex(): Response
    {
        $params = array();

        return $this->render('Admin/admin.html.twig', $params);
    }

    /**
     * List all cards currently implemented
     * @Route("/debugAllCards", name="debugAllCards")
     *
     * @return Response
     */
    public function debugAllCards(): Response
    {
        return $this->render('Admin/debugAllCards.html.twig');
    }

    /**
     * @Route("/deckPresetsFightSelection", name = "deckPresetsSelector")
     *
     * @return Response
     */
    public function deckPresetsFightSelection(): Response
    {
        $params = array();

        $params['presets'] = [];

        foreach (DeckPreset::cases() as $preset) {
            array_push($params['presets'], $preset->value);
        }

        return $this->render('Admin/presetFightSelector.html.twig', $params);
    }

    /**
     * @Route("/deckPresetsFightSelectionConfirmed", methods={"POST"}, name = "confirmDeckPresetsForFight")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function deckPresetsFightSelectionConfirmed(Request $request): Response
    {
        $presetA = $request->request->get('deckPresetA');
        $presetB = $request->request->get('deckPresetB');

        return $this->redirectToRoute('fightWithPreset', ['presetA' => $presetA, 'presetB' => $presetB]);
    }

    /**
     * @Route("/admin/error", name="adminError")
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function AdminExceptionTest(): Response
    {
        throw new \Exception('Erreur test!');
    }

    /**
     * @return bool
     */
    private function isAdmin() : bool
    {
        return $this->getUser()->getRoles()[0] === "ROLE_ADMIN";
    }
}