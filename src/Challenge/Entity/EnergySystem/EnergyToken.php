<?php

namespace App\Challenge\Entity\EnergySystem;

use App\Challenge\Enum\ElementType;

class EnergyToken implements IEnergySource
{
    private ElementType $element;

    /**
     * @param ElementType $element
     */
    public function __construct(ElementType $element)
    {
        $this->element = $element;
    }

    /**
     * @inheritDoc
     */
    public function getEnergyElement(): ElementType
    {
        return $this->element;
    }
}