<?php


namespace App\Service;

use App\Entity\User;
use Eternaltwin\Auth\AccessTokenAuthContext;
use Eternaltwin\Client\Auth;
use Eternaltwin\Client\HttpEtwinClient;
use Eternaltwin\OauthClient\RfcOauthClient;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class OAuthService
{
    /** @var UserServiceInterface  */
    private $userService;
    /** @var RfcOauthClient  */
    private $oauthClient;
    /** @var string  */
    private $etwinUri;

    public function __construct(
        string $etwinUri,
        string $authorizeUri,
        string $tokenUri,
        string $oauthCallback,
        string $clientId,
        string $clientSecret,
        UserServiceInterface $userService
    ) {
        $this->userService = $userService;
        $this->etwinUri = $etwinUri;
        $this->oauthClient = new RfcOauthClient(
            $authorizeUri,
            $tokenUri,
            $oauthCallback,
            $clientId,
            $clientSecret
        );
    }

    /**
     * Method that given a code ask to the eternal twin identity provider to get the user informations
     * If the user do not exist locally it create a new one
     */
    public function login(string $code): User
    {
        try {
            $accessToken = $this->oauthClient->getAccessTokenSync($code);
        } catch (GuzzleException $e) {
            throw new UnauthorizedHttpException($e->getMessage());
        } catch (\JsonException $e) {
            throw new UnauthorizedHttpException($e->getMessage());
        }

        $apiClient = new HttpEtwinClient($this->etwinUri);
        $apiUser = $apiClient->getSelf(Auth::fromToken($accessToken->getAccessToken()));

        if (!$apiUser instanceof AccessTokenAuthContext) {
            throw new \LogicException('Auth context not supported');
        }

        $userId = $apiUser->getUser()->getId()->getInner()->toString();
        $user = $this->userService->findUserByUserId($userId);

        if (null === $user) {
            $displayName = $apiUser->getUser()->getDisplayName()->getCurrent()->getValue()->toString();

            $isAdmin = false;
            if ($userId === $_ENV['ADMIN_ID']) {
                $isAdmin = true;
            }
            $user = $this->userService->createUser($userId, $displayName, $isAdmin);
        } else {
            $this->userService->reconnect($user);
        }

        return $user;
    }

    public function getAuthorizationUri(string $scope, ?string $state): string
    {
        return (string) $this->oauthClient->getAuthorizationUri($scope, $state ?? '');
    }
}
