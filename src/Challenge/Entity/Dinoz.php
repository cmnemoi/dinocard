<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\DinocardStat;
use App\Challenge\Enum\Keyword;

class Dinoz
{
    // Do we need an interface for that?
    private Card $owningCard;

    // Stats
    /** @var array<int> */
    private array $stats = array();

    /** @var array<array<int>> Dictionary of Array of Dinocard stats */
    private array $temporaryStats = array();

    private bool $isEgg = true;
    private bool $forcedDeath = false;

    /** @var array<Charm> */
    private array $charms = [];

    /**
     * @param Card $card
     */
    public function __construct(Card $card)
    {
        $this->owningCard = $card;

        $jsonData = $card->getExtraData();
        $requiredEclosion = $jsonData["dinozStats"]["hatchTime"];
        $strength = $jsonData["dinozStats"]["strength"];
        $endurance = $jsonData["dinozStats"]["endurance"];

        $this->stats[DinocardStat::RequiredEclosion->value] = $requiredEclosion;
        $this->stats[DinocardStat::Strength->value] = $strength;
        $this->stats[DinocardStat::Endurance->value] = $endurance;
        $this->stats[DinocardStat::Age->value] = 0;
        $this->stats[DinocardStat::HatchMarker->value] = 0;
        $this->stats[DinocardStat::InjureMarker->value] = 0;
        $this->stats[DinocardStat::Cost->value] = $card->countCost();
    }

    /**
     * @return Card
     */
    public function getOwningCard(): Card
    {
        return $this->owningCard;
    }

    /**
     * @param DinocardStat $stat
     *
     * @return int
     */
    public function getStat(DinocardStat $stat): int
    {
        return $this->stats[$stat->value];
    }

    public function setStat(DinocardStat $stat, int $value): void
    {
        $this->stats[$stat->value] = $value;
    }

    public function modifyStat(DinocardStat $stat, int $value): void
    {
        $this->stats[$stat->value] += $value;
    }

    /**
     * @return int
     */
    public function getHatchMarkerCount(): int
    {
        return $this->stats[DinocardStat::HatchMarker->value];
    }

    /**
     * @return int
     */
    public function getInjureMarkerCount(): int
    {
        return $this->stats[DinocardStat::InjureMarker->value];
    }

    /**
     * @param int $newCount
     */
    public function setInjureMarkerCount(int $newCount): void
    {
        $this->stats[DinocardStat::InjureMarker->value] = max(0, $newCount);
    }

    /**
     * @param int $mod
     */
    public function modifyInjureMarker(int $mod): void
    {
        $this->setInjureMarkerCount($this->stats[DinocardStat::InjureMarker->value] + $mod);
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return max($this->getStat(DinocardStat::Endurance) - $this->stats[DinocardStat::InjureMarker->value], 0);
    }

    /**
     * Calculates how many damages we apply on a dinoz and how many exceed the HP of the dinoz.
     *
     * Example:         A               B
     * Dmg :            4               4
     * Old Hp :         10              2
     * Reduced Dmg :    3               3
     * New Hp :         7               0
     * Applied Dmg :    3               2
     * Remaining Dmg :  0               1 (only > 0 if dinoz is dead after dmg application)
     *
     * @param int $damage
     *
     * @return array|int[] applied damage & remaining damage (for tenacious)
     */
    public function applyDamage(int $damage): array
    {
        $oldHealth = $this->getHealth();

        $reducedDamage = max(0, $damage - $this->getArmor());
        $this->modifyInjureMarker($reducedDamage);

        $newHealth = $this->getHealth();

        $appliedDamage = $oldHealth - $newHealth;
        $remainingDamage = max(0, $reducedDamage - $appliedDamage);

        return array($appliedDamage, $remainingDamage);
    }

    /**
     * @return bool
     */
    public function isEgg(): bool
    {
        return $this->isEgg;
    }

    /**
     * @param int $newCount
     */
    public function setHatchMarker(int $newCount): void
    {
        $this->stats[DinocardStat::HatchMarker->value] = max(0, $newCount);
    }

    /**
     * @param int $mod
     */
    public function modifyHatchMarker(int $mod): void
    {
        $this->setHatchMarker($this->stats[DinocardStat::HatchMarker->value] + $mod);
    }

    /**
     * @return bool
     */
    public function verifyHatchMarker(): bool
    {
        if ($this->stats[DinocardStat::HatchMarker->value] >= $this->getStat(DinocardStat::RequiredEclosion)) {
            $this->hatch();

            return true;
        }

        return false;
    }

    /**
     */
    public function invertHatch(): void
    {
        $this->stats[DinocardStat::HatchMarker->value] = 0;
        $this->isEgg = true;
    }

    /**
     */
    public function hatch(): void
    {
        $this->stats[DinocardStat::HatchMarker->value] = 0;
        $this->isEgg = false;
    }

    /**
     * @return bool
     */
    public function canAttack(): bool
    {
        return !(($this->isEgg() || $this->getOwningCard()->hasKeyword(Keyword::Stuck)));
    }

    /**
     * @return bool
     */
    public function isDead(): bool
    {
        $injureDeath = $this->getStat(DinocardStat::InjureMarker) >= $this->getStat(DinocardStat::Endurance);

        return $injureDeath || $this->forcedDeath;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $dinozId = $this->getOwningCard()->getId();

        $injureStatus = sprintf("  InjureMarkers : [%d/%d]", $this->getInjureMarkerCount(), $this->getStat(DinocardStat::Endurance));

        if ($this->isEgg()) {
            $eggHatchStatus = sprintf("HatchMarkers : [%d/%d]", $this->getHatchMarkerCount(), $this->getStat(DinocardStat::RequiredEclosion));
            $dinozAsString = sprintf("Egg : %d - %s - %s", $dinozId, $eggHatchStatus, $injureStatus);
        } else {
            $dinozAsString = sprintf("Dinoz : %d - Strength: %d - %s", $dinozId, $this->getStat(DinocardStat::Strength), $injureStatus);
        }

        foreach ($this->getCharms() as $charm) {
            $dinozAsString .= sprintf("<br> > is charmed by <b>the charm %d</b>", $charm->getOwningCard()->getId());
        }

        return $dinozAsString;
    }

    /**
     * @param Dinoz $dinozInDefense
     *
     * @return bool
     */
    public function canAttackDinoz(Dinoz $dinozInDefense): bool
    {
        if ($dinozInDefense->isEgg()) {
            return $this->getOwningCard()->hasKeyword(Keyword::Stompy);
        }
        if ($dinozInDefense->getOwningCard()->hasKeyword(Keyword::Vaporous)) {
            return false;
        }
        if ($this->getOwningCard()->hasKeyword(Keyword::Flying)) {
            return $dinozInDefense->getOwningCard()->hasKeyword(Keyword::Flying)
                || $dinozInDefense->getOwningCard()->hasKeyword(Keyword::Rock);
        }

        return true;
    }

    /**
     * TODO We could cache that everytime keywords changed on the owning Card.
     * O(n)
     * @return int
     */
    public function getArmor(): int
    {
        return $this->isEgg ? 0 : $this->getOwningCard()->countKeyword(Keyword::Armor);
    }

    /**
     * @param bool $forcedDeath
     */
    public function setForcedDeath(bool $forcedDeath): void
    {
        $this->forcedDeath = $forcedDeath;
    }

    /**
     * @param string     $sourceDescription
     * @param array<int> $stats             Should be [eclosion, strength, endurance, age]
     *
     * @return void
     */
    public function addTemporaryStat(string $sourceDescription, array $stats): void
    {
        $allStats = DinocardStat::cases();
        assert(count($stats) === count($allStats));

        if (array_key_exists($sourceDescription, $this->temporaryStats)) {
            $existingStats = &$this->temporaryStats[$sourceDescription];
            for ($i = 0; $i < count($existingStats); $i++) {
                $existingStats[$i] += $stats[$i];
            }
        } else {
            $this->temporaryStats[$sourceDescription] = $stats;
        }

        foreach ($allStats as $i => $stat) {
            $this->modifyStat($stat, $stats[$i]);
        }
    }

    /**
     * Remove all temporary stats from the specified source.
     * @param string $sourceDescription
     *
     * @return void
     */
    public function removeTemporaryStat(string $sourceDescription): void
    {
        if (array_key_exists($sourceDescription, $this->temporaryStats)) {
            $allStats = DinocardStat::cases();
            $existingStats = $this->temporaryStats[$sourceDescription];

            foreach ($allStats as $i => $stat) {
                $this->modifyStat($stat, -$existingStats[$i]);
            }

            unset($this->temporaryStats[$sourceDescription]);
        }
    }

    /**
     * Returns true if we got temporary stat from the specified source.
     * @param string $sourceDescription
     *
     * @return bool
     */
    public function hasTemporaryStatFromSource(string $sourceDescription): bool
    {
        return array_key_exists($sourceDescription, $this->temporaryStats);
    }

    /**
     * @return array
     */
    public function getCharms(): array
    {
        return $this->charms;
    }

    /**
     * @param Charm $charm
     *
     * @return void
     */
    public function addCharm(Charm $charm): void
    {
        assert($charm !== null);
        assert(false === in_array($charm, $this->charms, true));

        $this->charms[] = $charm;
        $charm->setAffectedDinoz($this);
    }

    /**
     * @param Charm $charm
     *
     * @return bool
     */
    public function removeCharm(Charm $charm): bool
    {
        if (null !== $charm) {
            $charmIndex = array_search($charm, $this->charms, true);
            assert($charmIndex !== false);
            unset($this->charms[$charmIndex]);

            return true;
        }

        return false;
    }
}
