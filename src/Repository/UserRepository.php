<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $role
     *
     * @return float|int|mixed|string
     */
    public function findAllByRole(string $role)
    {
        return $this->createQueryBuilder('u')
            ->where("JSON_GET_TEXT(u.roles,0) = :role")
            ->setParameter('role', $role)
            ->getQuery()
            ->getResult();
    }
}
