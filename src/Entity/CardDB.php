<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="card")
 */
class CardDB
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $cardId;

    /**
     * @ORM\Column(type="integer")
     */
    private int $amount = 0;

    /**
     * @ORM\ManyToOne(targetEntity="DeckDB", inversedBy="cards")
     */
    private DeckDB $deck;

    /**
     * @param string $cardId
     * @param int    $amount
     */
    public function __construct(string $cardId, int $amount)
    {
        $this->cardId = $cardId;
        $this->amount = $amount;
    }

    /**
     * @param DeckDB $deck
     */
    public function setDeck(DeckDB $deck): void
    {
        $this->deck = $deck;
    }

    /**
     * @return string
     */
    public function getCardId(): string
    {
        return $this->cardId;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }
}