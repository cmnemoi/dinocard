<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230609143220 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Replay Table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE replay_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE replay (id INT NOT NULL, first_opponent_id INT DEFAULT NULL, second_opponent_id INT DEFAULT NULL, first_deck_id INT DEFAULT NULL, second_deck_id INT DEFAULT NULL, seed INT DEFAULT 0 NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, won BOOLEAN DEFAULT false NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D937F4F2E190611E ON replay (first_opponent_id)');
        $this->addSql('CREATE INDEX IDX_D937F4F21680F0C2 ON replay (second_opponent_id)');
        $this->addSql('CREATE INDEX IDX_D937F4F22952420 ON replay (first_deck_id)');
        $this->addSql('CREATE INDEX IDX_D937F4F265BC8B1 ON replay (second_deck_id)');
        $this->addSql('ALTER TABLE replay ADD CONSTRAINT FK_D937F4F2E190611E FOREIGN KEY (first_opponent_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE replay ADD CONSTRAINT FK_D937F4F21680F0C2 FOREIGN KEY (second_opponent_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE replay ADD CONSTRAINT FK_D937F4F22952420 FOREIGN KEY (first_deck_id) REFERENCES deck (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE replay ADD CONSTRAINT FK_D937F4F265BC8B1 FOREIGN KEY (second_deck_id) REFERENCES deck (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE replay_id_seq CASCADE');
        $this->addSql('ALTER TABLE replay DROP CONSTRAINT FK_D937F4F2E190611E');
        $this->addSql('ALTER TABLE replay DROP CONSTRAINT FK_D937F4F21680F0C2');
        $this->addSql('ALTER TABLE replay DROP CONSTRAINT FK_D937F4F22952420');
        $this->addSql('ALTER TABLE replay DROP CONSTRAINT FK_D937F4F265BC8B1');
        $this->addSql('DROP TABLE replay');
    }
}
