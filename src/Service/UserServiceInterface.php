<?php

namespace App\Service;

use App\Entity\User;

/**
 * Interface UserServiceInterface
 * @package App\Service
 *
 * Provides helpful functions to manage the User entity
 */
interface UserServiceInterface
{
    public function save(User $user): User;

    public function findUserByUserId(string $userId): ?User;

    public function createUser(string $userId, string $displayName, bool $isAdmin = false): User;

    public function reconnect(User $user);
}