<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum RoundPhase: int
{
    case Start  = 0;
    case OpponentA = 1;
    case OpponentB = 2;
    case Attack = 3;
    case End = 4;

    /**
     * @param RoundPhase $currentTurn
     *
     * @return RoundPhase
     */
    public static function nextPhase(RoundPhase $currentTurn): RoundPhase
    {
        $nextPhaseIndex = ($currentTurn->value + 1) % (RoundPhase::End->value + 1);

        return RoundPhase::from($nextPhaseIndex);
    }
}