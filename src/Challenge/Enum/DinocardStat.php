<?php

namespace App\Challenge\Enum;

/**
 * Dinocard stat for Dinoz
 * https://php.watch/versions/8.1/enums
 */
enum DinocardStat: string
{
    case RequiredEclosion = "requiredEclosion";
    case Strength = "strength";
    case Endurance = "endurance";
    case Age = "age";
    case HatchMarker = "hatchMarker";
    case InjureMarker = "injureMarker";
    case Cost = "cost";
}