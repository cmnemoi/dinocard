<?php

namespace App\Entity;

use App\Repository\DeckRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DeckRepository::class)
 * @ORM\Table(name="deck")
 */
class DeckDB
{
    // TODO We need to identify easily the main element(s) of the decks

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * Many decks have one Owner. This is the owning side.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="decks")
     */
    private User $owner;

    /**
     * @ORM\OneToMany(targetEntity="CardDB", mappedBy="deck", cascade={"persist", "remove"})
     *
     * @var Collection<CardDB>
     */
    private Collection $cards;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="integer")
     */
    private int $victory = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private int $defeat = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private int $level = 1;

    /**
     * @ORM\Column(type="integer")
     */
    private int $experience = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $private = false;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $createdAt;

    // TODO DeckStats

    /**
     * @param User   $owner
     * @param string $name
     */
    public function __construct(User $owner, string $name)
    {
        $this->createdAt = new \DateTime();
        $this->cards = new ArrayCollection();
        $this->owner = $owner;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getOwner(): User
    {
        return $this->owner;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description ?: "";
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return Collection<CardDB>
     */
    public function getCards(): Collection
    {
        return $this->cards;
    }

    /**
     * @return int
     */
    public function getTotalCardsCount(): int
    {
        $count = 0;
        /** @var CardDB $card */
        foreach ($this->cards as $card) {
            $count += $card->getAmount();
        }

        return $count;
    }

    /**
     * @param CardDB $cardDB
     */
    public function addCard(CardDB $cardDB)
    {
        $this->cards->add($cardDB);
        $cardDB->setDeck($this);
    }

    /**
     * @param CardDB $cardDB
     */
    public function removeCard(CardDB $cardDB)
    {
        $this->cards->remove($cardDB);
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @return int
     */
    public function getExperience(): int
    {
        return $this->experience;
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->private;
    }

    /**
     * @param bool $private
     */
    public function setPrivate(bool $private): void
    {
        $this->private = $private;
    }

    /**
     * 
     */
    public function togglePrivate(): void
    {
        $this->private = !$this->private;
    }

    /**
     * @return float
     */
    public function getExperienceRatio(): float
    {
        // TODO Having a function that tells what is the nextLevel requirement.
        $nextLevel = 2 * $this->level;
        if ($nextLevel > 0) {
            return ($this->experience / $nextLevel) * 100.0;
        }

        return 0;
    }

    /**
     * @return int
     */
    public function getVictory(): int
    {
        return $this->victory;
    }

    /**
     * @return int
     */
    public function getDefeat(): int
    {
        return $this->defeat;
    }

    /**
     * @return float
     */
    public function getVictoryRatio(): float
    {
        $played = $this->victory + $this->defeat;
        if ($played > 0) {
            return ($this->victory / $played) * 100.0;
        }

        return 50;
    }
}