<?php

namespace App\Challenge\Entity;

/**
 * Stack of Cards
 */
class CardStack
{
    /** @var array<Card> */
    private array $cards = [];

    /**
     * @param Card $card
     */
    public function addCard(Card $card) : void
    {
        array_push($this->cards, $card);
    }

    /**
     *
     */
    public function shuffle() : void
    {
        shuffle($this->cards);
    }

    /**
     * @return array
     */
    public function getCards() : array
    {
        return $this->cards;
    }

    /**
     * @return Card|null
     */
    public function drawCard() : ?Card
    {
        if ($this->isEmpty()) {
            return null;
        }

        return array_pop($this->cards);
    }

    public function isEmpty(): bool
    {
        return 0 === count($this->cards);
    }

    /**
     * @return int
     */
    public function getStackSize() : int
    {
        return count($this->cards);
    }

    public function sortStack()
    {
        sort($this->cards);
    }
}