<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\Keyword;
use App\Challenge\Enum\DinocardStat;

/**
 */
class Board
{
    private const K_MAX_DINOZ_ON_BOARD = 3;

    /** @var array<Dinoz> */
    private array $dinozOnBoard = [];
    private bool $boardRequiresShift = false;
    private ?Dinoz $currentHero = null;

    // TODO Passive Items

    /**
     * @param Card $card
     *
     * @return bool
     */
    public function canAddDinozOnBoard(Card $card): bool
    {
        if ($card->hasKeyword(Keyword::Hero)) {
            return $this->currentHero === null;
        }

        if ($card->hasKeyword(Keyword::Overflow)) {
            return true;
        }

        $countOnBoard = count($this->dinozOnBoard);
        if ($this->currentHero) {
            $countOnBoard--;
        }

        return $countOnBoard < $this::K_MAX_DINOZ_ON_BOARD;
    }

    /**
     * @param Dinoz $dinoz
     */
    public function addDinoz(Dinoz $dinoz): void
    {
        if ($dinoz->getOwningCard()->hasKeyword(Keyword::Hero)) {
            $this->currentHero = $dinoz;
        }

        array_push($this->dinozOnBoard, $dinoz);
    }

    /**
     * @param Dinoz $dinoz
     * @param int   $dinozIndex
     *
     * @return void
     */
    public function replaceDinoz(Dinoz $dinoz, int $dinozIndex): void
    {
        $this->dinozOnBoard[$dinozIndex] = $dinoz;
    }

    /**
     * Remove the dinoz with the specified index
     * @param int $dinozIndex
     *
     * @return bool
     */
    public function removeDinoz(int $dinozIndex): bool
    {
        if (array_key_exists($dinozIndex, $this->dinozOnBoard)) {
            $removedDinoz = $this->dinozOnBoard[$dinozIndex];
            if ($this->currentHero === $removedDinoz) {
                $this->currentHero = null;
            }
            unset($this->dinozOnBoard[$dinozIndex]);

            $this->boardRequiresShift = true;

            return true;
        }

        return false;
    }

    /**
     * Shift all dino as left as possible (only if we removed at least one dinoz from the Board).
     */
    public function shiftDinozOnBoard(): void
    {
        if ($this->boardRequiresShift) {
            $this->boardRequiresShift = false;
            $this->dinozOnBoard = array_values($this->dinozOnBoard); // Reindex the array without gaps.
        }
    }

    public function hasDinoz(): bool
    {
        return count($this->dinozOnBoard) > 0;
    }

    /**
     * @return Dinoz[]
     */
    public function getDinoz(): array
    {
        return $this->dinozOnBoard;
    }

    /**
     * @return Dinoz[]
     */
    public function getEggs(): array
    {
        return $this->filterDinoz(function ($dinoz) {
            return $dinoz->isEgg();
        });
    }

    /**
     * @return Dinoz[]
     */
    public function getHatchedDinoz(): array
    {
        return $this->filterDinoz(function ($dinoz) {
            return !$dinoz->isEgg();
        });
    }

    /**
     * @param callable $filterCallback
     *
     * @return array
     */
    public function filterDinoz(callable $filterCallback): array
    {
        return array_filter($this->dinozOnBoard, $filterCallback);
    }

    /**
     * @param array $dinozList
     * @return array
     */
    public function getOldestDinozFromList(array $dinozList): array
    {
        $oldestDinoz = null;
        $oldestDinozIndex = 0;
        $oldestAge = 0;

        // TODO asort with sort callback by age
        // TODO What happens if we got two dinoz with same age?
        foreach ($dinozList as $index => $dinoz) {
            $age = $dinoz->getStat(DinocardStat::Age);
            if ($age > $oldestAge) {
                $oldestAge = $age;
                $oldestDinoz = $dinoz;
                $oldestDinozIndex = $index;
            }
        }

        return array($oldestDinoz, $oldestDinozIndex);
    }
}
