<?php

namespace App\Challenge\Entity\EnergySystem;

use App\Challenge\Enum\ElementType;
use App\Service\Utils;

/**
 * Stores the list of Energy sources and the current amount of energies
 */
class EnergyReserve
{
    /** @var int[] */
    private array $energiesCount = [];

    /**
     * Is visible on board, can be removed
     * @var IEnergySource[]
     */
    private array $energySourcesOnBoard = [];

    /**
     * Is not visible on board, can't be removed.
     * @var IEnergySource[]
     */
    private array $intangibleEnergySources = [];

    /**
     *
     */
    public function __construct()
    {
        foreach (ElementType::cases() as $energy) {
            $this->energiesCount[$energy->value] = 0;
        }
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $result = "Energies - ";
        foreach ($this->energiesCount as $key => $count) {
            $result .= sprintf('[%s %d]', Utils::getEnergyAsImage($key), $count);
        }

        return $result;
    }

    /**
     * @return int[]
     */
    public function getEnergiesCount(): array
    {
        return $this->energiesCount;
    }

    /**
     * @param ElementType $element
     *
     * @return int
     */
    public function getEnergyCount(ElementType $element): int
    {
        return $this->energiesCount[$element->value];
    }

    /**
     * @return int
     */
    public function getTotalEnergyCount(): int
    {
        return $this->energiesCount[ElementType::Neutral->value];
    }

    /**
     * @param IEnergySource $energySource
     * @param bool          $isIntangible
     */
    public function addEnergySource(IEnergySource $energySource, bool $isIntangible = false): void
    {
        if ($isIntangible) {
            $this->intangibleEnergySources[] = $energySource;
        } else {
            $this->energySourcesOnBoard[] = $energySource;
        }

        $this->modifyEnergyAmount($energySource->getEnergyElement(), 1);
    }

    /**
     * @param IEnergySource $energySource
     * @param bool          $isIntangible
     */
    public function removeEnergySource(IEnergySource $energySource, bool $isIntangible = false): void
    {
        if ($isIntangible) {
            if (($key = array_search($energySource, $this->intangibleEnergySources, true)) !== false) {
                array_splice($this->intangibleEnergySources, $key, 1);
                $this->modifyEnergyAmount($energySource->getEnergyElement(), -1);
            }
        } else {
            if (($key = array_search($energySource, $this->energySourcesOnBoard, true)) !== false) {
                array_splice($this->energySourcesOnBoard, $key, 1);
                $this->modifyEnergyAmount($energySource->getEnergyElement(), -1);
            }
        }
    }

    /**
     * @param ElementType $element
     * @param int         $count
     *
     * @return array<IEnergySource>
     */
    public function removeEnergyByElement(ElementType $element, int $count): array
    {
        $removedEnergySources = [];

        for ($i = 0; $i < count($this->energySourcesOnBoard) && count($removedEnergySources) < $count; $i++) {
            $energySource = $this->energySourcesOnBoard[$i];
            if ($energySource->getEnergyElement() === $element) {
                $removedEnergySources[] = $energySource;
                unset($this->energySourcesOnBoard[$i]);
            }
        }
        $this->energySourcesOnBoard = array_values($this->energySourcesOnBoard); // Reindex!
        $this->modifyEnergyAmount($element, -count($removedEnergySources));

        return $removedEnergySources;
    }

    /**
     * @param ElementType $element
     * @param int         $amount
     */
    private function modifyEnergyAmount(ElementType $element, int $amount)
    {
        if (0 !== $amount) {
            $this->energiesCount[ElementType::Neutral->value] += $amount;
            $this->energiesCount[$element->value] += $amount;

            /*
            if ($amount > 0) {
                // TODO Trigger OnGainedEnergy(amount)
            } else {
                // TODO Trigger OnLostEnergy(abs(amount))
            }
            */
        }
    }
}
