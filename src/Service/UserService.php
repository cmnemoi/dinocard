<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;

class UserService implements UserServiceInterface
{
    private EntityManagerInterface $entityManager;

    private UserRepository $repository;

    public function __construct(EntityManagerInterface $entityManager, UserRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    public function save(User $user): User
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    public function findUserByUserId(string $userId): ?User
    {
        return $this->repository->findOneBy(['userId' => $userId]);
    }

    public function createUser(string $userId, string $displayName, bool $isAdmin = false): User
    {
        $user = new User($userId, $displayName);
        if ($isAdmin) {
            $user->setRoles(['ROLE_ADMIN']);
        }
        $this->save($user);

        return $user;
    }

    public function reconnect(User $user)
    {
        $user->setLastConnectionDate(new \DateTime());
        $this->save($user);
    }
}