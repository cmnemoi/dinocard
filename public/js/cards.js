
const CACHE_END_OFFSET = "cache_".length;

let cardContainer = null;
let cards = [];
let cardDivs = [];
let filters = {
    // element
    "fire": true,
    "water": true,
    "wood": true,
    "thunder": true,
    "air": true,
    // type
    "dinoz": true,
    "spell": true,
    "charm": true,
    "object": true,
    // rarity
    "common": true,
    "uncommon": true,
    "rare": true,
}

window.onload = init;

class CardData
{
    constructor(id, divId, type, element, rarity, edition)
    {
        this.id = id;
        this.divId = divId;
        this.type = type;
        this.element = element;
        this.rarity = rarity;
        this.edition = edition;
    }
}

function init()
{
    cardContainer = document.getElementById("cardExchangeCollection");
    parseAllCards();
}

function parseAllCards()
{
    // TODO Sort them by element and reparse their ID!

    if (cardContainer) {
        cardDivs = cardContainer.getElementsByClassName("dinocard");
        for (let i = 0; i < cardDivs.length; i++) {
            let cardDiv = cardDivs[i];
            cards.push(parseCard(cardDiv, i));
        }
    }
}

/**
 * @return CardData
 */
function parseCard(cardDiv, divIndex)
{
    let cache = cardDiv.getElementsByClassName("cachecache")[0];
    let cardId = parseInt(cache.id.substring(CACHE_END_OFFSET));
    let type = cache.querySelector("#type_" + cardId).innerHTML;
    let element = cache.querySelector("#element_" + cardId).innerHTML;
    let rarity = cache.querySelector("#rarity_" + cardId).innerHTML;

    return new CardData(cardId, divIndex, type, element, rarity, "TODO");
}

//#region Filters

function toggleElementFilter(name)
{
    let newValue = toggleFilter(name);
    enableAllCardsWithElement(name.toLowerCase(), newValue);
}

function toggleTypeFilter(name)
{
    let newValue = toggleFilter(name);
    enableAllCardsWithType(name.toLowerCase(), newValue);
}

function toggleRarityFilter(name)
{
    let newValue = toggleFilter(name);
    enableAllCardsWithRarity(name.toLowerCase(), newValue);
}

function resetAllFilters()
{
    Object.keys(filters).forEach(key => {
        if (filters[key]) {
            filters[key] = true;
            refreshFilterButton(key.charAt(0).toUpperCase() + key.slice(1), true);
        }
    });

    enableAllCards();
}

/**
 * Toggle the specified filter
 * @param name
 * @returns {boolean}
 */
function toggleFilter(name)
{
    let nameLowCase = name.toLowerCase();

    let newToggleValue = !filters[nameLowCase];
    filters[nameLowCase] = newToggleValue;

    refreshFilterButton(name, newToggleValue);

    return newToggleValue;
}

/**
 * Refresh the button feedback.
 * @param name
 * @param newValue
 */
function refreshFilterButton(name, newValue)
{
    let button = document.getElementById("a" + name);
    if (button !== null) {
        let suffix = newValue ? "1" : "";
        button.className = "button" + name + suffix.toString();
    }
}

function enableAllCards()
{
    for (let i = 0; i < cards.length; i++) {
        cardDivs[cards[i].divId].style.display = "block";
    }
}

function enableAllCardsWithElement(element, enabled)
{
    for (let i = 0; i < cards.length; i++) {
        if (cards[i].element === element) {
            cardDivs[cards[i].divId].style.display = enabled && checkFilters(cards[i]) ? "block" : "none";
        }
    }
}
function enableAllCardsWithType(type, enabled)
{
    for (let i = 0; i < cards.length; i++) {
        if (cards[i].type === type) {
            cardDivs[cards[i].divId].style.display = enabled && checkFilters(cards[i]) ? "block" : "none";
        }
    }
}

function enableAllCardsWithRarity(rarity, enabled)
{
    for (let i = 0; i < cards.length; i++) {
        if (cards[i].rarity === rarity) {
            cardDivs[cards[i].divId].style.display = enabled && checkFilters(cards[i]) ? "block" : "none";
        }
    }
}

function checkFilters(cardData)
{
    return filters[cardData.element] && filters[cardData.type] && filters[cardData.rarity];
}

//#endregion