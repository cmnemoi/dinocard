<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\ElementType;

class ElementFilter extends TargetFilter
{
    private bool $isInverted;
    private ElementType $searchedElement;

    /**
     * @param Card  $card
     * @param array $input
     */
    public function __construct(ElementType $searchedElement, bool $inverted = false)
    {
        $this->searchedElement = $searchedElement;
        $this->isInverted = $inverted;
    }

    public function filter($targets){
        if ($this->isInverted) {
            $filter = fn($dinoz) => $dinoz->getOwningCard()->getEnergyElement() !== $this->searchedElement;
        } else {
            $filter = fn($dinoz) => $dinoz->getOwningCard()->getEnergyElement() === $this->searchedElement;
        }
        return array_filter($targets, $filter);
    }
}