<?php

namespace App\Challenge\Service;

use App\Challenge\Entity\Charm;
use App\Challenge\Entity\Dinoz;
use App\Challenge\Entity\Duel;
use App\Challenge\Entity\Effect;
use App\Challenge\Entity\EnergySystem\EnergyToken;
use App\Challenge\Entity\Opponent;
use App\Challenge\Enum\ActionType;
use App\Challenge\Enum\DinocardStat;
use App\Challenge\Enum\ElementType;
use App\Challenge\Enum\Keyword;
use App\Challenge\Enum\TargetEntity;
use App\Challenge\Enum\TargetQuantity;
use App\Challenge\Enum\TargetSide;
use App\Constants\LocaKey;

class EffectInterpreter
{
    // TODO Remove all array return value and use directly the LogService to add new logs.
    // TODO Migrate everything to Log V2
    // TODO Create an enum for all loca?

    private DuelSimulator $duelSimulator;
    private LogService $logService;

    /**
     * @param LogService $logService
     */
    public function __construct(LogService $logService)
    {
        $this->logService = $logService;
    }

    /**
     * @param DuelSimulator $duelSimulator
     */
    public function setDuelSimulator(DuelSimulator $duelSimulator): void
    {
        $this->duelSimulator = $duelSimulator;
    }

    /**
     * Trigger the specified effect.
     * @param Effect $effect
     *
     * @throws \Exception
     */
    public function computeEffect(Effect $effect): void
    {
        $duel = $this->duelSimulator->getDuel();
        $targets = self::computeEffectTargets($duel, $effect);

        match ($effect->getActionType()) {
            ActionType::Heal => EffectInterpreter::healEffect($duel, $effect, $targets),
            ActionType::Damage => EffectInterpreter::damageEffect($duel, $effect, $targets),
            ActionType::SetHP => EffectInterpreter::setHPEffect($duel, $effect, $targets),
            ActionType::Discard => EffectInterpreter::discardEffect($duel, $effect, $targets),
            ActionType::DestroyEnergy => EffectInterpreter::destroyEnergyEffect($duel, $effect, $targets),
            ActionType::AddEnergy => EffectInterpreter::addEnergyEffect($duel, $effect, $targets),
            //ActionType::SkipRoundPhase => throw new \Exception('To be implemented'),
            //ActionType::AlterGameConfig => throw new \Exception('To be implemented'),
            ActionType::PlayCard => EffectInterpreter::playCardEffect($duel, $effect, $targets),
            ActionType::ShuffleGraveyard => EffectInterpreter::shuffleGraveyardIntoDeckEffect($duel, $effect, $targets),
            //ActionType::Resurect => throw new \Exception('To be implemented'),
            //ActionType::SpawnToken => throw new \Exception('To be implemented'),
            ActionType::ChangeStat => EffectInterpreter::changeStatEffect($duel, $effect, $targets),
            ActionType::OverrideStat => EffectInterpreter::overrideStatEffect($duel, $effect, $targets),
            ActionType::TemporaryStat => EffectInterpreter::addTemporaryStatEffect($duel, $effect, $targets),
            //ActionType::StatMarker => throw new \Exception('To be implemented'),
            ActionType::HatchMarker => EffectInterpreter::hatchMarkerEffect($duel, $effect, $targets),
            ActionType::ForceHatch => EffectInterpreter::forceHatchEffect($duel, $effect, $targets),
            ActionType::BecomeEgg => EffectInterpreter::becomeEggEffect($duel, $effect, $targets),
            ActionType::Destroy => EffectInterpreter::destroyEffect($duel, $effect, $targets),
            ActionType::GainKeyword => EffectInterpreter::gainKeywordEffect($duel, $effect, $targets),
            ActionType::RemoveKeyword => EffectInterpreter::removeKeywordEffect($duel, $effect, $targets),
            //ActionType::GainControl => throw new \Exception('To be implemented'),

            default => throw new \Exception(sprintf("Effect Action (%s) not implemented", $effect->getActionType()->name)),
        };
    }

    /**
     * Compute the targets of the specified effect.
     * @param Duel   $duel
     * @param Effect $effect
     *
     * @return array<object>
     */
    public function computeEffectTargets(Duel $duel, Effect $effect): array
    {
        $targetQuantity = $effect->getTargetQuantity();

        if (TargetQuantity::CharmedOne === $targetQuantity) {
            assert($effect->getInstigator()->getCharm());
            assert($effect->getInstigator()->getCharm()->getAffectedDinoz());

            return array($effect->getInstigator()->getCharm()->getAffectedDinoz());
        }

        $targetSide = $effect->getTargetSide();
        $targetEntity = $effect->getTargetEntity();
        $targetFilter = $effect->getTargetFilter();

        return $this->computeTargets($duel, $targetSide, $targetEntity, $targetQuantity, $targetFilter);
    }

    /**
     * @param Duel  $duel
     * @param Charm $charm
     *
     * @return object|null
     */
    public function computeCharmTarget(Duel $duel, Charm $charm): ?object
    {
        $targets = $this->computeTargets($duel, $charm->getTargetSide(), $charm->getTargetEntity(), TargetQuantity::One, $charm->getTargetFilter());

        if (count($targets) > 0) {
            return $targets[0];
        }

        return null;
    }

    /**
     * Compute the targets with the specified info
     * @param Duel                  $duel
     * @param array<TargetSide>     $targetSide
     * @param array<TargetEntity>   $targetEntity
     * @param TargetQuantity        $targetQuantity
     * @param array<TargetFilter>   $targetFilter
     *
     * @return array<object>
     */
    private function computeTargets(Duel $duel, array $targetSide, array $targetEntity, TargetQuantity $targetQuantity, array $targetFilter = null): array
    {
        $targets = array();
        if (in_array(TargetSide::Ally, $targetSide)) {
            $targets = array_merge($targets, EffectInterpreter::computeTargetOpponent($duel->getCurrentOpponent(), $targetEntity));
        }
        if (in_array(TargetSide::Enemy, $targetSide)) {
            $targets = array_merge($targets, EffectInterpreter::computeTargetOpponent($duel->getEnemyOpponent(), $targetEntity));
        }

        //TODO Manage TargetQuantity::Neighbours

        if(count($targets) > 0) {
            if ($targetFilter) {
                foreach($targetFilter as $filter){
                    if($filter !== null) {
                        $targets = $filter->filter($targets);
                    }
                }
            }
        }

        if (TargetQuantity::One === $targetQuantity && count($targets) > 0) {
            //TODO Add smart Targeting, checking for the most relevant target
            $targets = array($targets[array_rand($targets)]);
        }

        return $targets;
    }

    /**
     * @param Opponent $opp
     * @param array    $targetEntity
     *
     * @return array
     */
    private function computeTargetOpponent(Opponent $opp, array $targetEntity): array
    {
        $targets = array();
        if (in_array(TargetEntity::Player, $targetEntity)) {
            $targets[] = $opp;
        }
        if (in_array(TargetEntity::Dinoz, $targetEntity)) {
            $targets = array_merge($targets, $opp->getBoard()->filterDinoz(fn($dinoz) => !$dinoz->isEgg() && !$dinoz->getOwningCard()->hasKeyword(Keyword::Bubble)));
        }
        if (in_array(TargetEntity::Egg, $targetEntity)) {
            $targets = array_merge($targets, $opp->getBoard()->getEggs());
        }
        if (in_array(TargetEntity::Item, $targetEntity)) {
            //TODO Fill when Items are implemented
        }
        if (in_array(TargetEntity::Charm, $targetEntity)) {
            foreach ($opp->getBoard()->getDinoz() as $dinoz) {
                $targets = array_merge($targets, $dinoz->getCharms());
            }
        }

        return $targets;
    }


    /**
     * Trigger a modification of the health thanks to the specified effect.
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @return array
     *
     * @throws \Exception
     */
    private function healEffect(Duel $duel, Effect $effect, array $targets): array
    {
        $healAmount = $effect->getActionParameter("value");
        if (null === $healAmount) {
            throw new \Exception(sprintf("Card %d has undefined action Parameter", $effect->getInstigator()->getId()));
        }

        $logs = [];
        foreach ($targets as $target) {
            if ($target instanceof Opponent) {
                $target->modifyHealth($healAmount);
                $this->logService->addTranslatedLog('FightLogs', 'effect.heal.player', ["%0%" => $target->getName(), "%1%" => $healAmount]);
            } elseif ($target instanceof Dinoz) {
                $target->modifyInjureMarker(-$healAmount);
                $this->logService->addTranslatedLog('FightLogs', 'effect.heal.dinoz', ["%0%" => $target->getOwningCard()->getId(), "%1%" => $healAmount]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }

        return $logs;
    }

    /**
     * Trigger a modification of the health thanks to the specified effect.
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     */
    private function damageEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $damageAmount = $effect->getActionParameter("value");
        if (null === $damageAmount) {
            throw new \Exception(sprintf("Card %d has undefined action Parameter", $effect->getInstigator()->getId()));
        }

        foreach ($targets as $target) {
            if ($target instanceof Opponent) {
                $target->modifyHealth(-$damageAmount);
                $this->logService->addTranslatedLog('FightLogs', 'effect.damage.player', ["%0%" => $target->getName(), "%1%" => $damageAmount]);
            } elseif ($target instanceof Dinoz) {
                list($appliedDamage, $remainingDamage) = $target->applyDamage($damageAmount);
                $this->logService->addTranslatedLog('FightLogs', 'effect.damage.dinoz', ["%0%" => $target->getOwningCard()->getId(), "%1%" => $appliedDamage]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * Trigger a modification of the health thanks to the specified effect.
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     */
    private function setHPEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $hpAmount = $effect->getActionParameter("value");
        if (null === $hpAmount) {
            throw new \Exception(sprintf("Card %d has undefined action Parameter", $effect->getInstigator()->getId()));
        }

        foreach ($targets as $target) {
            if ($target instanceof Opponent) {
                $target->setHealth($hpAmount);
                $this->logService->addTranslatedLog('FightLogs', 'effect.overrideHealth', ["%0%" => $target->getName(), "%1%" => $hpAmount]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * Trigger a discard thanks to the specified effect.
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     */
    private function discardEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $discardAmount = $effect->getActionParameter("value");
        if (null === $discardAmount) {
            throw new \Exception(sprintf("Card %d has undefined action Parameter", $effect->getInstigator()->getId()));
        }

        foreach ($targets as $target) {
            if ($target instanceof Opponent) {
                $target->discardTopDeck($discardAmount);
                $this->logService->addTranslatedLog('FightLogs', 'effect.discard', [
                    "%0%" => $target->getName(),
                    "%1%" => $discardAmount,
                    "%2%" => $target->getDeck()->getStackSize(),
                    "%3%" => count($target->getGraveyardCards()),
                ]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * Trigger an energy destruction thanks to the specified effect.
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     */
    private function destroyEnergyEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $destroyAmount = $effect->getActionParameter("value");
        if (null === $destroyAmount) {
            throw new \Exception(sprintf("Card %d has undefined action Parameter", $effect->getInstigator()->getId()));
        }

        //TODO Add an optional parameter to destroy energies of a certain type
        foreach ($targets as $target) {
            if ($target instanceof Opponent) {
                $removedEnergies = $target->removeRandomEnergiesInReserve($destroyAmount);
                for ($i = 0; $i < count($removedEnergies); $i++) {
                    $this->logService->addTranslatedLog('FightLogs', 'effect.energy.remove', ["%0%" => $target->getName(), "%1%" =>  $removedEnergies[$i]->getEnergyElement()->value]);
                }
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    private function addEnergyEffect(Duel $duel, Effect $effect, array $targets): void
    {
        //TODO Implement addEnergy
        foreach ($targets as $target) {
            if ($target instanceof Opponent) {
                $energyElement = ElementType::tryFrom($effect->getActionParameter("element"));

                $energyToken = new EnergyToken($energyElement);
                $target->getEnergyReserve()->addEnergySource($energyToken);

                $this->logService->addLog(sprintf("Add 1 energy %s", $energyElement->value));
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @return array
     *
     * @throws \Exception
     * Usage in JSON
        "action": {
            "type" : "playCard",
                "parameter": {
                "value": "2"
            }
        }
     */
    private function playCardEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $cardAmount = $effect->getActionParameter("value");
        if (null === $cardAmount) {
            throw new \Exception(sprintf("Card %d has undefined action Parameter", $effect->getInstigator()->getId()));
        }

        foreach ($targets as $target) {
            if ($target instanceof Opponent) {
                $this->logService->addTranslatedLog('FightLogs', 'effect.playCard.expected', ["%0%" => $target->getName(), "%1%" => $cardAmount]);
                $amountDrawn = 0;
                for ($i = 0; $i < $cardAmount; $i++) {
                    if ($this->duelSimulator->attemptCardDraw($target)) {
                        $amountDrawn++;
                    }
                }
                $this->logService->addTranslatedLog('FightLogs', 'effect.playCard.result', ["%0%" => $target->getName(), "%1%" => $amountDrawn]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    private function shuffleGraveyardIntoDeckEffect(Duel $duel, Effect $effect, array $targets)
    {
        foreach ($targets as $target) {
            if ($target instanceof Opponent) {
                $graveyardCards = $target->getGraveyardCards();
                $amountRestoredInDeck = count($graveyardCards);

                foreach ($graveyardCards as $card) {
                    $target->getDeck()->addCard($card);
                }
                $target->getDeck()->shuffle();

                $target->clearGraveyards();

                $this->logService->addTranslatedLog('FightLogs', 'effect.shuffleIntoGraveyard', [
                    "%0%" => $target->getName(),
                    "%1%" => $amountRestoredInDeck,
                ]);
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     * Usage in JSON
        "action": {
          "type" : "changeStat",
          "parameter" : {
            "endurance": 3
            "strength": -1
          }
        }
     */
    private function changeStatEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $statChange = $effect->getAllActionParameter();

        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                foreach ($statChange as $stat => $value) {
                    $target->modifyStat(DinocardStat::from($stat), $value);
                }
                $this->logService->addTranslatedLog('FightLogs', 'effect.stat.changed', [
                    "%0%" => $target->getOwningCard()->getId(),
                    "%1%" => $target->getStat(DinocardStat::Strength),
                    "%2%" => $target->getStat(DinocardStat::Endurance),
                ]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    private function addTemporaryStatEffect(Duel $duel, Effect $effect, array $targets)
    {
        $statChange = $effect->getAllActionParameter();

        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                $stats = [];
                foreach (DinocardStat::cases() as $key => $stat) {
                    $statString = $stat->value;
                    $stats[$key] = 0;
                    if (array_key_exists($statString, $statChange)) {
                        $stats[$key] = $statChange[$statString];
                    }
                }

                $target->addTemporaryStat("Card ".$effect->getInstigator()->getId(), $stats);

                $this->logService->addTranslatedLog('FightLogs', 'effect.stat.changed', [
                    "%0%" => $target->getOwningCard()->getId(),
                    "%1%" => $target->getStat(DinocardStat::Strength),
                    "%2%" => $target->getStat(DinocardStat::Endurance),
                ]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     *
     * Usage in JSON
        "action": {
          "type" : "overrideStat",
          "parameter" : {
            "endurance": 3
            "strength": -1
          }
        }
     */
    private function overrideStatEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $statChange = $effect->getAllActionParameter();

        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                foreach ($statChange as $stat => $value) {
                    $target->setStat(DinocardStat::from($stat), $value);
                }
                $this->logService->addTranslatedLog('FightLogs', 'effect.stat.changed', [
                    "%0%" => $target->getOwningCard()->getId(),
                    "%1%" => $target->getStat(DinocardStat::Strength),
                    "%2%" => $target->getStat(DinocardStat::Endurance),
                ]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     * Usage in JSON
        "action": {
            "type" : "forceHatch"
        }
     */
    private function forceHatchEffect(Duel $duel, Effect $effect, array $targets): void
    {
        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                $target->setHatchMarker($target->getStat(DinocardStat::RequiredEclosion));
                $this->logService->addTranslatedLog('FightLogs', 'effect.hatch.force', ["%0" => $target->getOwningCard()->getId()]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     *
     * Usage in JSON
        "action": {
            "type" : "hatchMarker"
            "parameter" : {
                "value": 2
            }
        }
     */
    private function hatchMarkerEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $value = $effect->getActionParameter("value");
        if (null === $value) {
            throw new \Exception(sprintf("Card %d has undefined action Parameter", $effect->getInstigator()->getId()));
        }
        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                $target->modifyHatchMarker($value);
                $this->logService->addTranslatedLog('FightLogs', 'effect.hatch.modify', ["%0%" => $target->getOwningCard()->getId(), "%1%" => $target->getHatchMarkerCount()]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     *
     * Usage in JSON
        "action": {
            "type" : "becomeEgg"
            "parameter :{
                "value": 0
            }
        }
     *  If Value >= 0, set Hatch Marker to that value
     *  If Value <0, set HatchMarker to RequiredEclosion - Value
     */
    private function becomeEggEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $value = $effect->getActionParameter("value");
        if (null === $value) {
            $value = 0;
        }

        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                if ($value >= 0) {
                    $newHatchMarkerCount = $value;
                } else {
                    $newHatchMarkerCount = $target->getStat(DinocardStat::RequiredEclosion) + $value;
                }
                $target->invertHatch();
                $target->setHatchMarker($newHatchMarkerCount);
                $this->logService->addTranslatedLog('FightLogs', 'effect.hatch.revert', ["%0" => $target->getOwningCard()->getId()]);
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     *
     * Usage in JSON
       "action": {
         "type" : "destroy"
       }
     */
    private function destroyEffect(Duel $duel, Effect $effect, array $targets): void
    {
        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                $target->setForcedDeath(true);
                $this->logService->addTranslatedLog('FightLogs', 'effect.destroy.dinoz', ["%0%" => $target->getOwningCard()->getId()]);
            } elseif ($target instanceof Charm) {
                $this->duelSimulator->destroyCharm($target);
                $this->logService->addTranslatedLog('FightLogs', 'effect.destroy.charm', ["%0%" => $target->getOwningCard()->getId()]);
            } elseif ($target instanceof Item) {
                //TODO Implement this when Items are Implemented
                $this->logService->addTranslatedLog('FightLogs', 'effect.destroy.item');
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     *
     * Usage in JSON
       "action": {
         "type" : "gainKeyword",
         "parameter" : {
           "value" : "poisoned"
         }
       }
     *  OR
       "action": {
         "type" : "gainKeyword",
         "parameter" : {
           "value" : ["poisoned","rock"]
         }
       }
     */
    private function gainKeywordEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $keywordParameter = $effect->getActionParameter("value");
        $keywords = [];
        if (is_array($keywordParameter)) {
            foreach ($keywordParameter as $kw) {
                $tmpKW = Keyword::from($kw);
                $keywords[] = $tmpKW;
            }
        } else {
            $keywords = array(Keyword::from($keywordParameter));
        }

        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                foreach ($keywords as $keyword) {
                    $target->getOwningCard()->addKeyword($keyword);
                    $this->logService->addTranslatedLog('FightLogs', 'effect.keyword.add', ["%0%" => $target->getOwningCard()->getId(), "%1%" => $keyword->name]);
                }
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }

    /**
     * @param Duel   $duel
     * @param Effect $effect
     * @param array  $targets
     *
     * @throws \Exception
     *
     * Usage in JSON
     *  "action": {
     *    "type" : "removeKeyword",
     *    "parameter" : {
     *      "value" : "poisoned"
     *    }
     *  }
     *  OR
     *  "action": {
     *    "type" : "removeKeyword",
     *    "parameter" : {
     *      "value" : ["poisoned","rock"]
     *    }
     *  }
     */
    private function removeKeywordEffect(Duel $duel, Effect $effect, array $targets): void
    {
        $keywordParameter = $effect->getActionParameter("value");
        $keywords = [];
        if (is_array($keywordParameter)) {
            foreach ($keywordParameter as $kw) {
                $tmpKW = Keyword::from($kw);
                $keywords[] = $tmpKW;
            }
        } else {
            $keywords = array(Keyword::from($keywordParameter));
        }

        foreach ($targets as $target) {
            if ($target instanceof Dinoz) {
                foreach ($keywords as $keyword) {
                    $target->getOwningCard()->removeKeyword($keyword);
                    $this->logService->addTranslatedLog('FightLogs', 'effect.keyword.remove', ["%0%" => $target->getOwningCard()->getId(), "%1%" => $keyword->name]);
                }
            } else {
                throw new \Exception(sprintf("%s not implemented for target class : %s ", __METHOD__, get_class($target)));
            }
        }
    }
}
