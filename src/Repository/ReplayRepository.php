<?php

namespace App\Repository;

use App\Entity\Replay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @ORM\Entity(repositoryClass=ReplayRepository::class)
 * @ORM\Repository
 */
class ReplayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Replay::class);
    }
}
