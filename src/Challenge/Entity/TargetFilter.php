<?php

namespace App\Challenge\Entity;
use App\Challenge\Enum\Keyword;
use App\Challenge\Enum\ElementType;
use App\Challenge\Enum\DinocardStat;
use App\Challenge\Enum\FilterOperator;

abstract class TargetFilter
{
    public function filter($targets){
        return $targets;
    }

    public static function initialize($filterJson){
        if (!is_array($filterJson)){
            $filterJson = array($filterJson);
        }
        if (array_key_exists("keyword", $filterJson)) {
            $keyword = Keyword::from($filterJson["keyword"]);
            return new KeywordFilter($keyword, array_key_exists("inverted", $filterJson));
        }
        if (array_key_exists("element", $filterJson)) {
            $element = ElementType::from($filterJson["element"]);
            return new ElementFilter($element, array_key_exists("inverted", $filterJson));
        }
        if (array_key_exists("stat", $filterJson)) {
            $stat = DinocardStat::from($filterJson["stat"]);
            $operator = FilterOperator::from($filterJson["operator"]);
            $value = 0;
            if (array_key_exists("value", $filterJson)) {
                $value = $filterJson["value"];
            }
            return new StatFilter($stat, $operator, $value);
        }
    }
}