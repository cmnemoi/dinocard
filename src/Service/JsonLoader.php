<?php

namespace App\Service;

use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Translation\Exception\InvalidResourceException;

/**
 * Heavily inspired by symfony/translation/Loader/JsonFileLoader
 */
class JsonLoader
{
    private KernelInterface $kernel;

    /**
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @param string $fileName
     *
     * @return array
     */
    public function loadJsonFromPublicFolder(string $fileName) : array
    {
        return $this->loadJsonFile('public'.DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.$fileName.'.json');
    }

    /**
     * Load a json file from Project Root.
     * @param string $fileName
     *
     * @return array
     */
    public function loadJsonFile(string $fileName) : array
    {
        $jsonArray = [];

        // https://stackoverflow.com/questions/6654157/how-to-get-a-platform-independent-directory-separator-in-php
        $jsonPath = $this->kernel->getProjectDir().DIRECTORY_SEPARATOR.$fileName;

        if ($data = file_get_contents($jsonPath)) {
            $jsonArray = json_decode($data, true);

            if (0 < $errorCode = json_last_error()) {
                throw new InvalidResourceException(sprintf('Error parsing JSON: %s', $this->getJSONErrorMessage($errorCode)));
            }
        }

        return $jsonArray;
    }

    /**
     * Translates JSON_ERROR_* constant into meaningful message.
     */
    private function getJSONErrorMessage(int $errorCode): string
    {
        switch ($errorCode) {
            case \JSON_ERROR_DEPTH:
                return 'Maximum stack depth exceeded';
            case \JSON_ERROR_STATE_MISMATCH:
                return 'Underflow or the modes mismatch';
            case \JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
            case \JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON';
            case \JSON_ERROR_UTF8:
                return 'Malformed UTF-8 characters, possibly incorrectly encoded';
            default:
                return 'Unknown error';
        }
    }
}