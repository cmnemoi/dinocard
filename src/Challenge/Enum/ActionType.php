<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum ActionType: string
{
    //Global
    case AlterGameConfig = "alterGameConfig";

    //Players
    case PlayCard = "playCard";
    case Discard   = "discard";
    case AddEnergy = "addEnergy";
    case DestroyEnergy  = "destroyEnergy";
    case ShuffleGraveyard = "shuffleGraveyard";
    case SpawnToken = "spawnToken";
    case Resurect = "Resurect";

    //Cards/Board
    case ChangeStat = "changeStat";
    case OverrideStat = "overrideStat";

    case TemporaryStat = "temporaryStats";
    case StatMarker = "statMarker";
    case HatchMarker = "hatchMarker";
    case ForceHatch = "forceHatch"; //Never used but makes sense to exist
    case BecomeEgg = "becomeEgg";
    case GainControl = "gainControl";
    case Destroy = "destroy";
    case GainKeyword = "gainKeyword";
    case RemoveKeyword = "removeKeyword";
    case SetHP    = "setHP";
    case Damage  = "damage";
    case Heal = "heal";


    /**
     * @param string|array $input
     * @return array
     */
    public static function parse(string|array $input): array
    {
        if (is_string($input)) {
            return array( ActionType::from($input));
        }
        $output = array();
        foreach ($input as $action) {
            array_push($output, ActionType::from($action));
        }

        return $output;
    }
}