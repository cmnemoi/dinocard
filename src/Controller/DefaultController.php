<?php

// src/Controller/DefaultController.php
namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;

class DefaultController extends AbstractController
{
    private ManagerRegistry $managerRegistry;

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * Index redirect to the appropriate homepage
     * @Route("/", name="homepage")
     */
    public function Index(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        return $this->render('index.html.twig');
    }

    /**
     * Route allowing to change the default locale!
     * @Route("/lang/{_locale}/", name="homepage_locale")
     * @param Request $request
     * @return Response
     */
    public function changeLanguage(Request $request): Response
    {
        $isConnected = $this->getUser() !== null;
        if ($isConnected) {
            return $this->redirect($request->headers->get('referer'));
        } else {
            // When not connected, the webpage is not translated since we rely on a session variable.
            return $this->forward("App\Controller\DefaultController::Index");
        }
    }

    /**
     * @Route("/card/viewAll", name="collection")
     * @return Response
     */
    public function collection(): Response
    {
        return $this->render('Card/collection.html.twig');
    }
}