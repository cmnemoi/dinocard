<?php

namespace App\Challenge\Entity;

use App\Challenge\Enum\ActionType;
use App\Challenge\Enum\TargetEntity;
use App\Challenge\Enum\TargetQuantity;
use App\Challenge\Entity\TargetFilter;
use App\Challenge\Enum\TargetSide;

class Effect
{
    private Card $instigator;

    /** @var array<TargetSide> */
    private array $targetSide;

    /** @var array<TargetEntity> */
    private array $targetEntity;


    /** @var array<TargetFilter> */
    private array $targetFilter;

    private TargetQuantity $targetQuantity;

    /** @var array<ActionType> */
    private array $actionType;

    private array $actionParameter = [];

    /**
     * @param Card  $card
     * @param array $input
     */
    public function __construct(Card $card, array $input)
    {
        $this->instigator = $card;
        $this->targetSide = TargetSide::parse($input["target"]["side"]);
        $this->targetEntity = TargetEntity::parse($input["target"]["entity"]);
        $this->targetQuantity = TargetQuantity::from($input["target"]["quantity"]);
        $this->targetFilter = array();
        if (array_key_exists("filter", $input["target"])) {
            $this->targetFilter = array();
            foreach($input["target"]["filter"] as $filterJson){
                array_push($this->targetFilter, TargetFilter::initialize($filterJson));
            }
        }
        
        $this->actionType = ActionType::parse($input["action"]["type"]);
        if (array_key_exists("parameter", $input["action"])) {
            $this->actionParameter = $input["action"]["parameter"];
        }
    }

    /**
     * @return ActionType
     */
    public function getActionType(): ActionType
    {
        return $this->actionType[0];
    }

    /**
     * @return array<TargetEntity>
     */
    public function getTargetEntity(): array
    {
        return $this->targetEntity;
    }

    /**
     * @return array<TargetSide>
     */
    public function getTargetSide(): array
    {
        return $this->targetSide;
    }

    /**
     * @return array<TargetFilter>
     */
    public function getTargetFilter(): array
    {
        return $this->targetFilter;
    }

    /**
     * @return array
     */
    public function getAllActionParameter(): array
    {
        return $this->actionParameter;
    }

    /**
     * @param String $key
     * @return int|string|array
     */
    public function getActionParameter(String $key): mixed
    {
        if (array_key_exists($key, $this->actionParameter)) {
            return $this->actionParameter[$key];
        }
        return null;
    }

    /**
     * @return TargetQuantity
     */
    public function getTargetQuantity(): TargetQuantity
    {
        return $this->targetQuantity;
    }

    /**
     * @return Card
     */
    public function getInstigator(): Card
    {
        return $this->instigator;
    }
}