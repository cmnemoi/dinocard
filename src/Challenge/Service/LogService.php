<?php

namespace App\Challenge\Service;

use App\Challenge\Entity\Dinoz;
use App\Challenge\Entity\Opponent;
use App\Challenge\Enum\DinocardStat;
use App\Challenge\Enum\Keyword;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Register all logs and provide handy log functions
 */
class LogService
{
    /** @var array<string> */
    private array $logs = [];
    private DuelSimulator $duelSimulator;
    private TranslatorInterface $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param DuelSimulator $duelSimulator
     */
    public function setDuelSimulator(DuelSimulator $duelSimulator): void
    {
        $this->duelSimulator = $duelSimulator;
    }

    /**
     * Log each Players HP
     */
    public function logOpponentsHP(): void
    {
        $this->addLog(sprintf("%s Health : %d - %s Health : %d", $this->duelSimulator->getDuel()->getFirstOpponent()->getName(), $this->duelSimulator->getDuel()->getFirstOpponent()->getHealth(), $this->duelSimulator->getDuel()->getSecondOpponent()->getName(), $this->duelSimulator->getDuel()->getSecondOpponent()->getHealth()));
    }

    /**
     * Log every dinoz in play
     */
    public function logBattlefield(): void
    {
        $this->logBoard($this->duelSimulator->getDuel()->getFirstOpponent());
        $this->logBoard($this->duelSimulator->getDuel()->getSecondOpponent());
    }

    /**
     * Log every Dinoz on a specific Board
     * @param Opponent $opponent
     *
     * @return void
     */
    private function logBoard(Opponent $opponent): void
    {
        foreach ($opponent->getBoard()->getDinoz() as $dinozString) {
            $this->addPlayerLog($dinozString, $opponent);
        }
    }

    /**
     * @return void
     */
    public function logAllDinozCardsInPlay(): void
    {
        $firstOpponentDinozList = $this->duelSimulator->getDuel()->getFirstOpponent()->getBoard()->getDinoz();
        $secondOpponentDinozList = $this->duelSimulator->getDuel()->getSecondOpponent()->getBoard()->getDinoz();
        $maxAmountOfDinozInPlay = max(count($firstOpponentDinozList), count($secondOpponentDinozList));
        for ($i = 0; $i < $maxAmountOfDinozInPlay; $i++) {
            if (array_key_exists($i, $firstOpponentDinozList)) {
                $this->logDinoz($firstOpponentDinozList[$i], $this->duelSimulator->getDuel()->getFirstOpponent());
            }

            if (array_key_exists($i, $secondOpponentDinozList)) {
                $this->logDinoz($secondOpponentDinozList[$i], $this->duelSimulator->getDuel()->getSecondOpponent());
            }

            $this->addBlankLog(3);
        }
    }

    public function logDinoz(Dinoz $dinoz, ?Opponent $controller = null): void
    {
        $revealCardString = !$dinoz->isEgg() ? "FACE" : "BACK";
        if (null === $controller) {
            $controller = $this->duelSimulator->getDuel()->getCurrentOpponent();
        }
        $currentOpponent = $this->duelSimulator->getDuel()->getFirstOpponent() === $controller ? "A" : "B";

        $cardId = $dinoz->getOwningCard()->getId();
        // TODO Add a Display of Hatch Markers and Injure Markers on dinoz
        /*
        $markerCount = 0;
        if ($dinoz->isEgg()) {
            $markerCount = $dinoz->getHatchMarkerCount();
        } else {
            $markerCount = $dinoz->getInjureMarkerCount();
        }
        */

        $this->addLog(sprintf("SHOWDINOZ:%s:%s:%d:%d:%d", $revealCardString, $currentOpponent, $cardId, $dinoz->getStat(DinocardStat::Strength), $dinoz->getStat(DinocardStat::Endurance)));
    }

    /**
     * Log the command : "SHOWCARD:RevealCard:Opponent:CardID"
     * @param int           $cardId
     * @param bool          $revealCard
     * @param Opponent|null $opponent
     */
    public function addCardLog(int $cardId, bool $revealCard, ?Opponent $opponent = null): void
    {
        $revealCardString = $revealCard ? "FACE" : "BACK";
        if (null === $opponent) {
            $opponent = $this->duelSimulator->getDuel()->getCurrentOpponent();
        }
        $currentOpponent = $this->duelSimulator->getDuel()->getFirstOpponent() === $opponent ? "A" : "B";

        $this->addLog(sprintf("SHOWCARD:%s:%s:%d", $revealCardString, $currentOpponent, $cardId));
    }

    /**
     * @param string $log
     */
    public function addLog(string $log): void
    {
        array_push($this->logs, $log);
    }

    /**
     * @param int $count
     *
     * @return void
     */
    public function addBlankLog(int $count = 1): void
    {
        for ($i = 0; $i < $count; $i++) {
            $this->addLog("");
        }
    }

    /**
     * Highlight the specified capacity and log a message
     *
     * @param Keyword $keyword
     * @param string  $log
     */
    public function addOldCapacityLog(Keyword $keyword, string $log): void
    {
        $this->addLog(sprintf("<b style='color: green'>%s capacity</b> : %s", $keyword->name, $log));
    }

    /**
     * @param string    $log
     * @param ?Opponent $opponent
     */
    public function addPlayerLog(string $log, ?Opponent $opponent = null): void
    {
        $this->addLog(sprintf("%s%s", $this->getOpponentLogPrefix($opponent), $log));
    }

    /**
     * Log every energies
     */
    public function logEnergyReserve(): void
    {
        $this->addLog($this->duelSimulator->getDuel()->getCurrentOpponent()->getEnergyReserve());
    }

    /**
     * @return array
     */
    public function getLogs(): array
    {
        return $this->logs;
    }

    /**
     * @return void
     */
    public function addWinnerLog(): void
    {
        $winner = $this->duelSimulator->getDuel()->getWinner();
        if ($winner) {
            $this->addTranslatedLog('FightLogs', 'result.winner', ['%0' => $winner->getName()]);
        } else {
            $this->addTranslatedLog('FightLogs', 'result.draw');
        }
    }

    /**
     * @return void
     */
    public function addRoundStartLog(): void
    {
        $roundCount = $this->duelSimulator->getDuel()->getTurnCount();
        $this->addLog(sprintf("Turn <b style='color: green'>%d</b>", $roundCount));
    }

    /**
     * @param Keyword $keyword
     * @param array   $params
     *
     * @throws \Exception
     */
    public function addCapacityLog(Keyword $keyword, array $params): void
    {
        $keywordName = $keyword->name;
        $log = match ($keyword) {
            Keyword::Discharge => sprintf("Removed one %s energy", $params[0]),
            Keyword::Armor => throw new \Exception('To be implemented'),
            Keyword::Assassin => sprintf("%dth Dinoz assassinated the dinoz in %dth position.", $params[0], $params[1]),
            Keyword::Berserk => throw new \Exception('To be implemented'),
            Keyword::Bubble => throw new \Exception('To be implemented'),
            Keyword::Brood => throw new \Exception('To be implemented'),
            Keyword::Overflow => throw new \Exception('To be implemented'),
            Keyword::Poisoned => sprintf("%dth Dinoz took 1 damage.", $params[0]),
            Keyword::Stuck => throw new \Exception('To be implemented'),
            Keyword::Ephemeral => sprintf("%dth Dinoz auto destroyed.", $params[0]),
            Keyword::Eternal => throw new \Exception('To be implemented'),
            Keyword::Focus => "Card added to the EnergyReserve",
            Keyword::Hero => throw new \Exception('To be implemented'),
            Keyword::Initiative => throw new \Exception('To be implemented'),
            Keyword::Pack => throw new \Exception('To be implemented'),
            Keyword::Coward => sprintf("Discarded the %dth dinoz + Transfered %d hatch Markers", $params[0], $params[1]),
            Keyword::Stompy => throw new \Exception('To be implemented'),
            Keyword::Regeneration => sprintf("%dth Dinoz healed 1 damage.", $params[0]),
            Keyword::Rock => throw new \Exception('To be implemented'),
            Keyword::Wizard => sprintf("Player lost one %s energy", $params[0]),
            Keyword::Survivor => throw new \Exception('To be implemented'),
            Keyword::Tenacious => sprintf("%dth Dinoz will apply %d damage on player", $params[0], $params[1]),
            Keyword::Vaporous => throw new \Exception('To be implemented'),
            Keyword::Venomous => sprintf("%dth Dinoz poisoned the dinoz in %dth position.", $params[0], $params[1]),
            Keyword::Flying => throw new \Exception('To be implemented'),

            default => sprintf("Working on it"),
        };
        $this->addLog(sprintf("<b style='color: green'>%s capacity</b> : %s", $keywordName, $log));
    }

    public function addAttackPhaseLogs(): void
    {
        $this->addLog("Battlefield :");
        if ($this->duelSimulator->getDuel()->isAnyDinozOnBattleField()) {
            $this->logBattlefield();

            $this->logAllDinozCardsInPlay();

            $this->addTranslatedLog('FightLogs', 'phase.attack');
        } else {
            $this->addLog("...");
        }

        $this->addBlankLog();
    }

    /**
     * @param string $string #translation
     * @return void
     */
    public function addErrorLog(string $string): void
    {
        $this->addLog("<b style='color: #b00e0e'>[ERROR]".$string."</b>");
    }

    public function addEmptyDeckLog(): void
    {
        $this->addLog(sprintf(" can't draw. Deck is empty. (%d)", $this->duelSimulator->getDuel()->getTurnSinceNoDrawAvailable()));
    }

    public function addTryToPlayCardLog(int $cardId, string $cardCost): void
    {
        if ($this->duelSimulator->getDuel()->isFirstOpponentTurn()) {
            $this->addLog(sprintf("Try to play the card %d (Cost: %s)", $cardId, $cardCost));
        }
    }

    public function addSendCardAsEnergyLog(string $element): void
    {
        $this->addLog(sprintf("Can't play it. Add the card as <b>%s Energy</b>", $element));
    }

    public function addNoPlaceOnBoardLog(): void
    {
        $this->addLog("DISCARD card - not enough space on the board!");
    }

    public function addDinozEnterBoardLog(Dinoz $enteredDinoz): void
    {
        if ($this->duelSimulator->getDuel()->isFirstOpponentTurn()) {
            $this->addLog(sprintf(
                "New Dinoz added to the board - (%d / %d / %d)",
                $enteredDinoz->getStat(DinocardStat::RequiredEclosion),
                $enteredDinoz->getStat(DinocardStat::Strength),
                $enteredDinoz->getStat(DinocardStat::Endurance)
            ));
        } else {
            $this->addLog("new Dinoz added to the board - ???");
        }
    }

    public function addHatchLog(int $dinozPosition, int $dinozId): void
    {
        $this->addLog(sprintf("<b>%dth Dinoz (%d) just Hatched!</b>", $dinozPosition, $dinozId));
    }

    public function logRoundEnd(): void
    {
        $this->logOpponentsHP();
        $this->addLog("LINE");
        $this->addBlankLog();
    }

    public function addDinozAttackLog(Dinoz $dinozInDefense, int $boardIndex, int $dinozInDefenseIndex, int $appliedDamage, Opponent $attacker): void
    {
        $defenderDinozForm = $dinozInDefense->isEgg() ? "Egg" : "Dinoz";
        if ($boardIndex === $dinozInDefenseIndex) {
            $this->addPlayerLog(
                sprintf(
                    "%dth Dinoz attacked the %s in front of him and dealt <b style='red'>%d damage(s)</b>.",
                    $boardIndex + 1,
                    $defenderDinozForm,
                    $appliedDamage
                ),
                $attacker
            );
        } else {
            $this->addPlayerLog(
                sprintf(
                    "%dth Dinoz attacked the %s in %dth position and dealt <b style='red'>%d damage(s)</b>.",
                    $boardIndex + 1,
                    $defenderDinozForm,
                    $dinozInDefenseIndex + 1,
                    $appliedDamage
                ),
                $attacker
            );
        }
    }

    public function addDinozAttackedPlayerLog(int $boardIndex, int $damage, Opponent $attacker): void
    {
        $this->addPlayerLog(sprintf("%dth Dinoz attacked the other Player and dealt <b style='red'>%d damage(s)</b>.", $boardIndex + 1, $damage), $attacker);
    }

    public function addDinozDiedLog(int $dinozIndex, Opponent $opponent): void
    {
        $this->addPlayerLog(sprintf("<b>%dth Dinoz just died.</b>", $dinozIndex + 1), $opponent);
    }

    public function addHatchStatusLog(Dinoz $egg): void
    {
        $this->addLog(sprintf("Dinoz eclosion counter : <b>%d/%d</b>", $egg->getHatchMarkerCount(), $egg->getStat(DinocardStat::RequiredEclosion)));
    }

    /**
     * @param string $domain     #TranslationDomain
     * @param string $key        #TranslationKey
     * @param array  $parameters #parameter
     */
    public function addTranslatedLog(string $domain, string $key, array $parameters = []): void
    {
        $this->addLog($this->translator->trans($key, $parameters, $domain));
    }

    /**
     * @param Opponent|null $opponent
     *
     * @return string
     */
    private function getOpponentLogPrefix(?Opponent $opponent): string
    {
        if (null === $opponent) {
            $opponent = $this->duelSimulator->getDuel()->getCurrentOpponent();
        }

        if ($this->duelSimulator->getDuel()->getFirstOpponent() === $opponent) {
            $colorStyle = "<b style='color: #b00e0e'>";
        } else {
            $colorStyle = "<b style='color: #1c1cbb'>";
        }

        return sprintf("%s%s</b> : ", $colorStyle, $opponent->getName());
    }

    /**
     * @param int $seed
     *
     * @return void
     */
    public function addShareableFightLink(string $fightUrl): void
    {
        $this->addLog(sprintf("Share the battle : <a href=\"%s\">%s</a>", $fightUrl, $fightUrl));
    }
}
