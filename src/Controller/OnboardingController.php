<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Replay;
use App\Repository\UserRepository;
use App\Repository\ReplayRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OnboardingController extends AbstractController
{
    private ManagerRegistry $managerRegistry;

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @Route("/user/{id?}", name="user")
     *
     * @param int $id
     *
     * @return Response
     */
    public function user(?int $id): Response
    {
        /** @var UserRepository $userRepository */
        if ($userRepository = $this->managerRegistry->getRepository(User::class)) {
            $user = $id ? $userRepository->find($id) : $this->getUser();

            if($user) {
                return $this->render('Onboarding/user.html.twig', ['user' => $user]);
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/ranks/{page?1}", name="ranks")
     *
     * @return Response
     */
    public function leaderboard(int $page): Response
    {
        /** @var UserRepository $userRepository */
        if ($userRepository = $this->managerRegistry->getRepository(User::class)) {
            $users = $userRepository->findBy(array(), array(), 20, ($page-1) * 20);

            /** @var User $a */
            usort($users, fn($a, $b) => $a->getPoints() < $b->getPoints());

            // TODO Verify if we can directly get the value of page
            return $this->render('Onboarding/ranks.html.twig', ['users' => $users, 'page' => $page]);
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/history/{id?}", name="history")
     *
     * @param int|null $id
     *
     * @return Response
     */
    public function history(?int $id): Response
    {
        /** @var UserRepository $userRepository */
        if ($userRepository = $this->managerRegistry->getRepository(User::class)) {
            $user = $id ? $userRepository->find($id) : $this->getUser();

            if ($user) {
                /** @var ReplayRepository $replayRepository */
                if ($replayRepository = $this->managerRegistry->getRepository(Replay::class)) {
                    $replays = $replayRepository->findBy(['firstOpponent' => $user], ['createdAt' => 'DESC'], 20, 0);

                    return $this->render('Onboarding/history.html.twig', ['replays' => $replays, 'user' => $user]);
                }
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/help")
     *
     * @return Response
     */
    public function help(): Response
    {
        return $this->render('Onboarding/help.html.twig');
    }
}