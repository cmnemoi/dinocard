
/** Dinocard original script */
function updateType()
{
    let type = document.getElementById("type").value;
    let divd = document.getElementById("divDinoz");

    if (type === "1") {
        divd.style.visibility = "visible";
    } else {
        divd.style.visibility = "hidden";
    }
}

function select2select(sel1, sel2)
{
    let select1 = document.getElementById(sel1);
    let select2 = document.getElementById(sel2);
    let value = '';
    let text = '';
    let title = '';

    if (select1.selectedIndex > -1) {
        for (let i = 0; i < select1.length; i++) {
            if (select1.options[i].selected) {
                value = select1.options[i].value;
                text = select1.options[i].text;
                title = select1.options[i].title;
                add(select2, value, text, title);
            }
        }
    }
}


function checkElements(type)
{
    let tab = [];
    tab[0] = document.getElementById("feu");
    tab[1] = document.getElementById("bois");
    tab[2] = document.getElementById("eau");
    tab[3] = document.getElementById("foudre");
    tab[4] = document.getElementById("ciel");
    let count = 0;
    for (let i = 0; i < 5; i++) {
        if (tab[i].checked) {
            count++;
        }
    }
    if (count > 2) {
        let found = false;
        let j = 0;
        while ((!found) && (j < 5)) {
            let elem = tab[j];
            if ((elem.id !== document.getElementById(type).id) && (elem.checked)) {
                elem.checked = false;
                found = true;
            }
            j++;
        }
    }
}


function remove(sel1)
{
    let select1 = document.getElementById(sel1);
    if (select1.selectedIndex > -1) {
        for (let i = 0; i < select1.length; i++) {
            if (select1.options[i].selected) {
                for (let n = i; n < select1.length - 1; n++) {
                    select1.options[n].text = select1.options[n + 1].text;
                    select1.options[n].value = select1.options[n + 1].value;
                    select1.options[n].title = select1.options[n + 1].title;
                    select1.options[n].selected = !select1.options[n + 1].selected;
                }
                i--;
                select1.length--;
            }
        }
    }
}


function add(sel, value, text, title)
{
    sel.length++;
    for (let n = sel.length - 1; n > 0; n--) {
        sel.options[n].value = sel[n - 1].value;
        sel.options[n].text = sel[n - 1].text;
        sel.options[n].title = sel[n - 1].title;
    }

    sel.options[0].value = value;
    sel.options[0].text = text;
    sel.options[0].title = title;
}


function resetVictories()
{
    if (confirm("Remettre à zéro les stats de victoires des cartes  ?")) {
        document.location = "/partyTest/resetVictories";
    }
}


function submitCard(f, back)
{
    let form = document.getElementById(f);
    let name = document.getElementById("name").value;
    let type = document.getElementById("type").value;
    let pound = document.getElementById("pound").value;
    let level = document.getElementById("level").value;
    let strength = document.getElementById("strength").value;
    let endurance = document.getElementById("endurance").value;
    let validity = true;

    if (name === "") {
        validity = false;
        alert("Le nom de la carte n'est pas défini");
    }

    if (type === "dinoz") {
        if (strength !== null) {
            if (isNaN(strength)) {
                validity = false;
                alert("La force n'est pas une valeur numérique");
            }
        } else {
            validity = false;
            alert("aucune valeur pour la force");
        }
        if (endurance !== null) {
            if (isNaN(endurance)) {
                validity = false;
                alert("L'endurance n'est pas une valeur numérique");
            }
        } else {
            validity = false;
            alert("aucune valeur pour l'endurance");
        }
    }
    if (pound !== null) {
        if (isNaN(pound)) {
            validity = false;
            alert("Le poids statistique n'est pas une valeur numérique");
        }
    } else {
        validity = false;
        alert("aucune valeur pour le poids statistique");
    }

    if (validity) {
        if (back !== "") {
            let b = document.getElementById("back");
            b.value = back;
        }
        form.submit();
    }

}


function submitTournament(formId)
{
    let form = document.getElementById(formId);
    let name = document.getElementById("name");
    if (name.innerHTML === "") {
        alert("Le nom du tournoi n'est pas défini");
    } else {
        form.submit();
    }
}


function submitCapacity(form)
{
    let name = document.getElementById("name").value;
    let val = document.getElementById("value").value;
    let validity = true;

    if (validity) {
        form.submit();
    }
}


function addOneCard(id)
{
    let nb = document.getElementById("nb_" + id);
    nb.innerHTML++;
}


function addInstance(id, max)
{
    let nb = document.getElementById("nb_" + id);
    if (nb.innerHTML < max) {
        nb.innerHTML++;
    }
}


function removeCard(id)
{
    let nb = document.getElementById("nb_" + id);
    if (nb.innerHTML > 0) {
        nb.innerHTML--;
    }
}


/**
 * @param form form that contains the checkbox to check
 */
function checkAllCheckboxes(form)
{
    for (let i = 0; i < form.elements.length; i++) {
        let e = form.elements[i];

        if ((e.type === 'checkbox') && (!e.disabled)) {
            e.checked = form.active_check_all.checked === true;
        }
    }
}


/**
 * Coche toutes les checkboxes de type remove
 * @param form formulaire dont il faut cocher toutes les checkboxes
 */
function rm_checkAll(form)
{
    for (let i = 0; i < form.elements.length; i++) {
        let e = form.elements[i];

        if ((e.type === 'checkbox') && (!e.disabled) && (e.name.indexOf("rm_", 0) !== -1)) {
            e.checked = form.rm_active_check_all.checked === true;
        }
    }
}


/**
 * Coche toutes les checkboxes de type publique
 * @param form formulaire dont il faut cocher toutes les checkboxes
 */
function pub_checkAll(form)
{
    for (let i = 0; i < form.elements.length; i++) {
        let e = form.elements[i];

        if ((e.type === 'checkbox') && (!e.disabled) && (e.name.indexOf("pub_", 0) !== -1)) {
            e.checked = form.pub_active_check_all.checked === true;
        }
    }
}




function mass(formId)
{
    if (confirm("Peter le joueur de cartes (copyright Lolo) ?")) {
        let form = document.getElementById(formId);
        form.action = "/card/massCards";
        form.submit();
    }
}


//gestion style css du formulaire addEditCapacity pour la m�thode add_capacity
function switchValue(form, id)
{
    let method = document.getElementById(id + "method");
    let divValue = document.getElementById(id + "value");
    let divCapList = document.getElementById(id + "capacityList");
    let divDinozList = document.getElementById(id + "dinozList");

    if ((method.value === 6) || (method.value === 12)) {
        divValue.style.visibility = "hidden";
        divDinozList.style.visibility = "hidden";
        divCapList.style.visibility = "visible";
    } else {
        if (method.value === 8) {
            divValue.style.visibility = "hidden";
            divDinozList.style.visibility = "visible";
            divCapList.style.visibility = "hidden";
        } else {
            divValue.style.visibility = "visible";
            divDinozList.style.visibility = "hidden";
            divCapList.style.visibility = "hidden";
        }
    }
}


//switch visible/hidden sur le style d'un objet
function reveal(id, height)
{
    let obj = document.getElementById(id);
    if (obj !== null) {
        if (obj.style.visibility === "visible") {
            obj.style.visibility = "hidden";
            obj.style.height = "0px";
        } else {
            if (obj.style.visibility === "hidden") {
                obj.style.visibility = "visible";
                obj.style.height = height + "px";
            }
        }
    }
}


//switch visible/hidden sur le style d'un objet
function revealSearch(id)
{
    let obj = document.getElementById(id);
    if (obj !== null) {
        if (obj.style.visibility === "visible") {
            obj.style.visibility = "hidden";
            obj.style.height = "0px";
        } else {
            if (obj.style.visibility === "hidden") {
                obj.style.visibility = "visible";
                obj.style.height = "260px";
            }
        }
    }
}


function newSwap(formId)
{
    let form = document.getElementById(formId);
    form.action = "/swap/new";
    form.submit();
}

function setExch(formId, maxTokens)
{
    let form = document.getElementById(formId);
    let state = document.getElementById("state");
    let nbCards = document.getElementById("nbCards");
    document.getElementById("tok").value = document.getElementById("ttokens").value;
    document.getElementById("bonusCards").value = document.getElementById("temp").innerHTML;
    let tokens = document.getElementById("tok").value;

    if (isNaN(tokens)) {
        alert("Le nombre de tokens n'est pas valide.");
    } else {
        if (tokens > maxTokens) {
            alert("Le nombre de tokens proposé est supérieur à ce que vous possédez.");
        } else {
            if (state.value === "new") {
                if ((nbCards.innerHTML === 0) && (tokens <= 0)) {
                    alert("Une nouvelle proposition d'échange doit être composée d'au moins une carte ou un token. ");
                } else {
                    let response = document.form.content;
                    if (response.value === "") {
                        alert("Vous ne pouvez pas créer de nouvelle proposition d'échange sans message associé.");
                    } else {
                        document.getElementById("response").value = response.value;
                        form.action = "/exchange/saveNew";
                        form.submit();
                    }
                }
            }
            if (state.value === "modify") {
                form.action = "/exchange/saveModification";
                form.submit();
            }
        }
    }
}

/*
 function filterNewExch(formId) {
 let form = document.getElementById(formId) ;
 let state = document.getElementById("state") ;
 if (state ==="new") {
 let resp = document.getElementById("resp") ;
 let response = document.getElementById("response") ;
 resp.value = response.value ;
 } else
 form.submit() ;
 }
 */


function submitFight(form)
{
    let html = document.getElementById("dispHtml");
    let xml = document.getElementById("dispXml");
    let flash = document.getElementById("dispFlash");
    let seed = document.getElementById("seed");

    if (seed.value !== "") {
        if (isNaN(seed.value)) {
            seed.value = "";
        }
    }

    if (html.checked) {
        form.action = "/partyTest/html";
    } else {
        if (xml.checked) {
            form.action = "/partyTest/testXml";
        } else {
            if (flash.checked) {
                form.action = "/partyTest/flash";
            }
        }
    }
    form.submit();
}


function addStarter(formId)
{
    let form = document.getElementById(formId);
    form.action = "/user/addStarter";
    form.submit();
}


function addCard(formId)
{
    let exm = document.getElementById("exemplaire");
    if (isNaN(exm.value)) {
        alert("le nombre d'exemplaires n'est pas un nombre.");
    } else {
        let form = document.getElementById(formId);
        form.action = "/card/addCard";
        form.submit();
    }
}


function duel(formId, id)
{
    let form = document.getElementById(formId);
    let chosen = document.getElementById("chosenOpp");
    chosen.value = id;
    form.submit();
}


function confirmResetFights()
{
    if (confirm("Remettre à zéro le nombre de parties jouées par jour ? ")) {
        document.location = "/pack/resetPF";
    }
}


function cancelExch(idExch, idu, name)
{
    if (confirm("Annuler définitivement cet échange avec " + name + " ?")) {
        let form = document.getElementById("form");
        form.action = "/exchange/cancel";
        form.submit();
    }
}

function confirmExch(idExch, idu, name, nbOther, nbYou, tokOther, tokYou)
{
    if (!((nbOther <= 0) && (nbYou <= 0) && (tokOther <= 0) && (tokYou <= 0))) {
        let text = "Valider l'échange avec " + name + " ?";
        if ((nbOther === 0) && (tokOther === 0)) {
            text += "\nATTENTION : " + name + " ne vous propose ni carte, ni token. Assurez vous que vous voulez bien donner vos cartes/tokens sans rien attendre en échange.";
        }

        if (confirm(text)) {
            let form = document.getElementById("form");
            form.action = "/exchange/confirm";
            form.submit();
        }
    } else {
        alert("L'échange ne peut pas être validé. Aucune carte ou token n'est proposé de chaque côté.");
    }
}


function sortCards(attr, d)
{
    let filter = document.getElementById("ffilter");
    let sort = document.getElementById("sort");
    let desc = document.getElementById("desc");
    sort.value = attr;
    if (d === "t") {
        desc.value = "t";
    } else {
        desc.value = "f";
    }
    if ((document.form !== null) && (filter.fcontent !== null)) {
        filter.fcontent.value = document.form.content.value;
    }
    if ((filter.ftokens !== null) && (document.getElementById("ttokens"))) {
        filter.ftokens.value = document.getElementById("ttokens").value;
    }
    if (document.getElementById('temp') !== null) {
        filter.bc.value = document.getElementById('temp').innerHTML;
    }

    filter.submit();
}


function sortPackCards(attr, d)
{
    let filter = document.getElementById("ffilter");
    let sort = document.getElementById("sort");
    let desc = document.getElementById("desc");
    sort.value = attr;
    if (d === "t") {
        desc.value = "t";
    } else {
        desc.value = "f";
    }
    if (document.getElementById('temp') !== null) {
        filter.bc.value = document.getElementById('temp').innerHTML;
    }

    filter.submit();
}


function resetFilter()
{
    let filter = document.getElementById("ffilter");
    document.getElementById("if").value = "";
    if (document.getElementById("bc") !== null) {
        document.getElementById("bc").value = document.getElementById('temp').innerHTML;
    }
    filter.submit();
}


function resetInfo()
{
    let filter = document.getElementById("ffilter");
    document.getElementById("if").value = "";
    document.getElementById("reset").value = "1";

    filter.submit();
}


function goToCard(cardId)
{
    document.location = "/card/view?id=" + cardId;
}


function duplicateCard(id)
{
    let input = document.getElementById("duplicate");
    if (isNaN(input.value)) {
        alert("A vue de nez, le numéro d'édition est un peu chelou.");
    } else {
        document.location = "/card/duplicate?id=" + id + "&edition=" + input.value;
    }
}


function newEffect()
{
    let show = document.getElementById("sNewEffect");
    let form = document.getElementById("fcardEdit");
    let type = show.options[show.selectedIndex].value;
    let container = document.getElementById("show");
    container.value = type;
    form.action = "/card/edit#newEffect";
    form.submit();
}

function showDojoInfo(domainName, faceInfo, txt)
{
    //let d = document.getElementById('dojoInfo');

    let swf = new FlashObject(domainName + '/swf/portrait.swf', 'game', 60, 84, 7, 'cccccc');
    swf.addVariable('url', domainName + '/swf/avatar.swf');
    swf.addVariable('face', faceInfo);
    swf.addParam('wmode', 'transparent');
    swf.addParam('menu', 'false');

    let d = document.getElementById('dojoInfo');
    d.innerHTML = "<div class='borderWhite'><div id='dojoAvatar'></div>" + txt + "</div>";
    swf.write("dojoAvatar");
    d.style.display = 'block';

}

function moodSelector(domainName, faceInfo, mood)
{

    let swf = new FlashObject(domainName + '/swf/portrait.swf', 'game', 120, 168, 7, 'FFFFFF');
    swf.addVariable('url', domainName + '/swf/avatar.swf');
    swf.addVariable('flip', '2');
    swf.addVariable('face', faceInfo);
    swf.addParam('wmode', 'transparent');


    let d = document.getElementById('avatarMood');
    d.innerHTML = "<input class='button' type='hidden' name='mood' value='" + mood + "'/>";
    swf.write("forumAvatar");
    d.style.display = 'block';
}

function addACard(cardId)
{
    let name = document.getElementById("name" + cardId);
    if (name === null) {
        return;
    }
    if (confirm("Ajouter " + name.innerHTML + " à votre pack ? ")) {
        document.getElementById("cid").value = cardId;
        document.getElementById("ac").submit();
    }
}


function removeWish(cardId)
{
    let name = document.getElementById("name" + cardId);
    if (name === null) {
        return;
    }
    if (confirm("Retirer " + name.innerHTML + " de votre liste de recherche ? ")) {
        document.getElementById("rmWish").value = cardId;
        document.getElementById("fwishList").submit();
    }
}


function hideDojoInfo()
{
    let d = document.getElementById('dojoInfo');
    if (d) {
        d.style.display = 'none';
    }
}

function showUserMenu(id, nb)
{
    nbFriends = parseInt(nb);
    let d = document.getElementById('friendMenu' + id);

    for (let i = 0; i <= nbFriends; i++) {
        if (document.getElementById('friendMenu' + i)) {
            document.getElementById('friendMenu' + i).style.display = 'none';
        }
    }

    if (d) {
        d.style.display = 'block';
    }
}

/**
 * Get a value as string?
 * @param v
 * @returns {string}
 */
function scoreInt(v)
{
    let s = v;
    if (s === null) {
        return "null";
    }
    let inv = Math.floor(s);
    let sign = "";
    if (inv < 0) {
        sign = "-";
        inv = -1 * inv;
    }

    return sign + inv;
}

/**
 * Get a number as a list of images
 * @param fonts
 * @param n
 * @returns {string}
 */
function numberToImage(fonts, n)
{
    let str = scoreInt(n);
    let res = "";
    for (let i = 0; i < str.length; ++i) {
        let c = str.charAt(i);
        res = res + "<img alt=\"";
        res = res + c;
        res = res + "\" src=\"";
        res = res + fonts;
        res = res + "/";
        if (c === ".") {
            res = res + "dot";
        } else {
            res = res + c;
        }
        res = res + ".gif\"/>";
    }

    return res;
}

let zoomcard = false;
let zoomId = '';

function showMeDaCard(idCard)
{
    //alert(idCard+'   '+zoomcard);

    if (!zoomcard) {
        if (document.getElementById(idCard)) {
            document.getElementById(idCard).style.zIndex = '10';
            document.getElementById(idCard).style.marginTop = '-30px';
            zoomcard = true;
            zoomId = idCard;

        }

    } else {
        if (document.getElementById(zoomId)) {
            document.getElementById(zoomId).style.zIndex = '1';
            document.getElementById(zoomId).style.marginTop = '10px';
            zoomcard = false;
        }
    }
}


function switchExchangeView()
{
    document.getElementById('exc').value = document.getElementById('temp').innerHTML;
    document.getElementById('swtokens').value = document.getElementById('ttokens').value;
    document.getElementById('form').submit();
}


function submitView()
{
    document.getElementById('bonusCards').value = document.getElementById('temp').innerHTML;
    document.getElementById('form').submit()
}


function submitExchView()
{
    document.getElementById('bonusCards').value = document.getElementById('temp').innerHTML;
    document.getElementById('tok').value = document.getElementById('ttokens').value;
    document.getElementById('fExchTemp').submit()
}


function submitFilterNewExch()
{
    setToFilterNewExch();
    document.getElementById("ffilter").submit();
}

function setToFilterNewExch()
{
    document.getElementById("ftokens").value = document.getElementById('ttokens').value;
    document.getElementById("bc").value = document.getElementById('temp').innerHTML;
    document.getElementById("fcontent").value = document.getElementById("form").content.value;
}


function filterColSwap(id)
{
    let elem = document.getElementById(id);
    if (elem !== null) {
        if (elem.value === "1") {
            elem.value = "";
        } else {
            elem.value = "1";
        }
        let aImg = document.getElementById("a" + id);
        aImg.className = "button" + id + elem.value;
    }
}


function launchDuel()
{
    document.fStrategy.bfc.value = document.getElementById('temp').innerHTML;
    document.fStrategy.submit();
}


function exchangeInterface(value)
{
    document.getElementById('ex').value = value;
    document.getElementById('ffilter').submit();
}


function switchStartList(cardId, add)
{
    let txt = "Retirer cette carte des listes de départ ?";
    if (add) {
        txt = "Ajouter cette carte aux listes de départ ? ";
    }
    if (confirm(txt)) {
        document.location = "/card/switchStartList?id=" + cardId;
    }
}


function setPackDesc()
{
    let text = document.getElementById("desc");
    let t = text.value;
    if (t.length > 220) {
        text.value = t.substring(0, 220);
    }
}


function setLevelUp(type)
{
    let info = document.getElementById(type);
    let oldNb = 0;
    let nb = 0;
    if (info !== null) {
        oldNb = parseInt(info.innerHTML);
        nb = oldNb + 1
        info.innerHTML = nb;
    }
    let end = "";
    switch (type) {
        case 1 :
            end = "les cartes bonus.";
            break;
        case 2 :
            end = "les points de vie.";
            break;
        case 3 :
            end = "la compétence Stratégie.";
            break;
        case 4 :
            end = "la compétence Perception.";
            break;
        case 5 :
            end = "le  nombre de parties quotidiennes.";
            break;
    }
    if (confirm("Cliquez sur Ok pour augmenter définitivement " + end)) {
        let cInput = document.getElementById("c");
        cInput.value = type;
        document.getElementById("fLevelUp").submit();
    } else {
        if (info !== null) {
            info.innerHTML = oldNb;
        }
    }
}


function addDojoCard()
{
    if (confirm("Ajouter cette carte à votre pack ? ")) {
        let form = document.getElementById("addCard");
        form.submit();
    }
}

function changeTokens(max, type)
{
    let tok = document.getElementById("ttokens");
    if ((isNaN(tok.value)) || (tok.value === "")) {
        tok.value = "0";
    }
    let v = parseInt(tok.value);
    if ((type === "sum") && (v < max)) {
        tok.value = v + 1;
    }
    if ((type === "sub") && (v > 0)) {
        tok.value = v - 1;
    }
}


function ban(id, t)
{
    let form = document.getElementById("banForm");
    document.getElementById("banId").value = id;
    if (t === "1") {
        document.getElementById("banT").value = t;
    }
    document.getElementById("banWhy").value = prompt("Raison du ban : ", "");
    form.submit();
}