<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum TargetQuantity: string
{
    case One  = "one"; // Target will be a random one among all compute entities (or the most relevant in some case (NYI) ).
    case All = "all"; // Target will be all compute entities
    case CharmedOne = "charmedOne"; // Target will be the charmed Dinoz (the one affected by the Instigating Charm card).
    case Neighbours = "neighbours";
}