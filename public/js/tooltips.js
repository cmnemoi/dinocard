var ie=document.all
var w3c=document.getElementById && !document.all
var xOffset		= 80;
var yOffset		= 40;
var tipWidth	= 160;
var picWidth	= 36;

var enabletip=false
if (ie||w3c) {
	var tipobj
}


/*------------------------------------------------------------------------
SHOWS TOOLTIP
------------------------------------------------------------------------*/
function showTip(obj,title,contentBase){
	if (w3c||ie){

		var content = "<p>"+contentBase+"</p>"
		
		var curX=findPosX(obj)
		var curY=findPosY(obj)
		
		tipobj						= getRef("tooltip");
		tipcontent				= getRef("tooltipContent");
		tipobj.className	= "normalTip";
		
		var winwidth=ie&&!window.opera? ieTrueBody().clientWidth : window.innerWidth-90
		var winheight=ie&&!window.opera? ieTrueBody().clientHeight : window.innerHeight-90
		
		var leftedge = ( findPosX(obj) - tipWidth*0.7 + picWidth*0.7 + xOffset );
		var rightedge = leftedge + tipWidth;
		var topedge = ( findPosY(obj) + picWidth*1.1 + yOffset);
		var bottomedge = topedge - 100;
		
		//if the horizontal distance isn't enough to accomodate the width of the context menu
		if (rightedge>winwidth){
			//move the horizontal position of the menu to the left by it's width
				tipobj.style.left = ( curX - tipWidth ) +"px";
		}
		else {
			if (leftedge<0) {
				tipobj.style.left="5px"
			}
			else{
				//position the horizontal position of the menu where the mouse is positioned
				tipobj.style.left=leftedge+"px"
			}
		}

		//same concept with the vertical position
		if (bottomedge<tipobj.offsetHeight) {
			tipobj.style.top=curY-tipobj.offsetHeight-yOffset+"px"
		}
		else{
			tipobj.style.top=curY+yOffset+"px"
		}
		

		if (typeof title!="undefined") {
			if ( contentBase!="" ) {
				content='<div class="title">'+title+'</div>'+content
			}
			else {
				content='<div class="title">'+title+'</div>'+content
			}
		}
		tipcontent.innerHTML = content
		enabletip=true

		return false
	}
}


/*------------------------------------------------------------------------
SHOWS A SPECIAL TOOLTIP
------------------------------------------------------------------------*/
function showSpecialTip(obj,cssClass,title, contentBase) {
	showTip(obj,title,contentBase);
	tipobj.className = cssClass;
}


/*------------------------------------------------------------------------
HELP LINK
------------------------------------------------------------------------*/
function showHelp(obj,contentBase) {
	showTip(obj,"",contentBase);
	tipobj.className	= "helpTip";
	tipobj.style.left	= findPosX(obj) +"px";
	tipobj.style.top	= ( findPosY(obj) + 16 )+"px";
}


/*------------------------------------------------------------------------
HIDE THE TOOLTIP
------------------------------------------------------------------------*/
function hideTip(){
	if (w3c||ie){
		enabletip=false
//        tipobj.style.visibility="hidden" // avoid the IE6 cache optimisation with hidden blocks
		tipobj.style.top="-1000px"
		tipobj.style.backgroundColor=''
		tipobj.style.width=''
	}
}


/*------------------------------------------------------------------------
ONMOVE EVENT
------------------------------------------------------------------------*/
function moveTip(e){
	if (enabletip){
		var nondefaultpos=false
		var curX=(w3c)?e.pageX : event.x+ieTrueBody().scrollLeft;
		var curY=(w3c)?e.pageY : event.y+ieTrueBody().scrollTop;
		//Find out how close the mouse is to the corner of the window
		var winwidth=ie&&!window.opera? ieTrueBody().clientWidth : window.innerWidth-20
		var winheight=ie&&!window.opera? ieTrueBody().clientHeight : window.innerHeight-20

		var rightedge=ie&&!window.opera? winwidth-event.clientX-xOffset : winwidth-e.clientX-xOffset
		var bottomedge=ie&&!window.opera? winheight-event.clientY-yOffset : winheight-e.clientY-yOffset

		var leftedge=(xOffset<0)? xOffset*(-1) : -1000

		//if the horizontal distance isn't enough to accomodate the width of the context menu
		if (rightedge<tipobj.offsetWidth){
			//move the horizontal position of the menu to the left by it's width
			tipobj.style.left=curX-tipobj.offsetWidth+"px"
			nondefaultpos=true
		}
		else {
			if (curX<leftedge) {
				tipobj.style.left="5px"
			}
			else{
				//position the horizontal position of the menu where the mouse is positioned
				tipobj.style.left=curX+xOffset+"px"
			}
		}

		//same concept with the vertical position
		if (bottomedge<tipobj.offsetHeight) {
			tipobj.style.top=curY-tipobj.offsetHeight-yOffset+"px"
			nondefaultpos=true
		}
		else{
			tipobj.style.top=curY+yOffset+"px"
		}
	}
}

//document.onmouseover=moveTip


/*------------------------------------------------------------------------
BLOCK DISPLAY MANAGEMENT
------------------------------------------------------------------------*/
function show(block) {
	block.style.display="block";
}
function hide(block) {
	block.style.display="none";
}
function isVisible(block) {
	return block.style.display!="none";
}
function isHidden(block) {
	return block.style.display=="none";
}
function getVisibility(block) {
	return block.style.display;
}



function showBlock(name) {
	var block = getRef(name);
	show(block);
}
function hideBlock(name) {
	var block = getRef(name);
	hide(block);
}


/*------------------------------------------------------------------------
MISC FUNCTIONS
------------------------------------------------------------------------*/
function ieTrueBody(){
	return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function getRef(name) {
	return document.all? document.all[name] : document.getElementById? document.getElementById(name) : ""
}


/*------------------------------------------------------------------------
FINDS ABSOLUTE POSITION OF AN HTML OBJECT
------------------------------------------------------------------------*/
function findPosX(obj) {
	var curleft = 0;
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curleft += obj.offsetLeft
			obj = obj.offsetParent;
		}
	}
	else{
		if (obj.x) {
			curleft += obj.x;
		}
	}
	return curleft;
}

function findPosY(obj) {
	var curtop = 0;
	if (obj.offsetParent) {
		while (obj.offsetParent) {
			curtop += obj.offsetTop
			obj = obj.offsetParent;
		}
	}
	else {
		if (obj.y) {
			curtop += obj.y;
		}
	}
	return curtop;
}
