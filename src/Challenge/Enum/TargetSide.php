<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum TargetSide: string
{
    case Ally  = "ally";
    case Enemy = "enemy";

    /**
     * @param string|array $input
     * @return array
     */
    public static function parse(string|array $input): array
    {
        if (is_string($input)) {
            return array( TargetSide::from($input));
        }
        $outputSide = array();
        foreach ($input as $side) {
            array_push($outputSide, TargetSide::from($side));
        }
        return $outputSide;
    }
}