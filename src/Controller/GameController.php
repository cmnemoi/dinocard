<?php

namespace App\Controller;

use App\Challenge\Entity\Opponent;
use App\Challenge\Enum\DeckPreset;
use App\Challenge\Service\DuelSimulator;
use App\Entity\DeckDB;
use App\Entity\Replay;
use App\Entity\User;
use App\Repository\DeckRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 */
class GameController extends AbstractController
{
    private const DUMP_SEED = false;
    private const DUMP_DATA = false;

    private bool $shouldSaveReplay = false;
    private bool $shouldShowResultPageLink = false;

    private ManagerRegistry $managerRegistry;
    private DuelSimulator $fightSimulator;

    /**
     * @param ManagerRegistry $managerRegistry
     * @param DuelSimulator   $fightSimulator
     */
    public function __construct(ManagerRegistry $managerRegistry, DuelSimulator $fightSimulator)
    {
        $this->managerRegistry = $managerRegistry;
        $this->fightSimulator = $fightSimulator;
    }

    /**
     * @Route("/debugGeneration/{preset}", name="debugGeneration")
     *
     * @param string $preset
     *
     * @return Response
     */
    public function debugDeckGeneration(string $preset): Response
    {
        // TODO Simplify this logic, this is very complicated...
        $opponent = $this->fightSimulator->prepareOpponentWithPreset($preset, DeckPreset::from($preset));
        $opponent->getDeck()->sortStack();
        $params = ["data" => $opponent];

        if ($this::DUMP_DATA) {
            dump($opponent->getDeck());
        }

        return $this->render('Admin/debugCard.html.twig', $params);
    }

    /**
     * Start a random fight between presets.
     * @Route("/fight/random/{seed}", name="fightRandom")
     *
     * @param int $seed
     *
     * @return Response
     */
    public function fightRandom(int $seed = 0): Response
    {
        $this->initializeFight($seed);

        $randomPresetA = DeckPreset::getRandom();
        $opponentA = $this->fightSimulator->prepareOpponentWithPreset("A", $randomPresetA);
        $randomPresetB = DeckPreset::getRandom();
        $opponentB = $this->fightSimulator->prepareOpponentWithPreset("B", $randomPresetB);

        $this->fightSimulator->setFightUrl(sprintf("/fight/random/%d", $this->fightSimulator->getSeed()));

        return $this->renderFight($opponentA, $opponentB, $randomPresetA->name, $randomPresetB->name);
    }

    /**
     * @Route("/fight/preset/{presetA}/{presetB}/{seed}", name="fightWithPreset")
     *
     * @param string $presetA
     * @param string $presetB
     * @param int    $seed
     *
     * @return Response
     */
    public function fightPresets(string $presetA, string $presetB, int $seed = 0): Response
    {
        $this->initializeFight($seed);

        $opponentA = $this->fightSimulator->prepareOpponentWithPreset("A", DeckPreset::tryFrom($presetA));
        $opponentB = $this->fightSimulator->prepareOpponentWithPreset("B", DeckPreset::tryFrom($presetB));

        $this->fightSimulator->setFightUrl(sprintf("/fight/preset/%s/%s/%d", $presetA, $presetB, $this->fightSimulator->getSeed()));

        return $this->renderFight($opponentA, $opponentB, $presetA, $presetB);
    }

    /**
     * Test one of our deck against a random preset.
     * @Route("/fight/testDeck/{id}/{seed}", name="fightWithDeck")
     *
     * @param int $id
     * @param int $seed
     *
     * @return Response
     */
    public function fightTestDeck(int $id, int $seed = 0): Response
    {
        /** @var User $user */
        if ($user = $this->getUser()) {
            // Case 1 : No Seed, ckeck if we own the deck ($id) to trigger the fight.
            if (0 === $seed) {
                $deckDB = $user->findDeck($id);
            } else { // Case 2 : There is a seed, anyone can replay the fight!
                /** @var DeckRepository $deckRepository */
                $deckRepository = $this->managerRegistry->getManager()->getRepository(DeckDB::class);
                $deckDB = $deckRepository->find($id);
            }

            if ($deckDB) {
                $this->initializeFight($seed);

                $opponentA = $this->fightSimulator->prepareOpponentWithDeck($user->getDisplayName(), $deckDB);
                $randomPreset = DeckPreset::getRandom();
                $opponentB = $this->fightSimulator->prepareOpponentWithPreset("B", $randomPreset);

                $this->fightSimulator->setFightUrl(sprintf("/fight/testDeck/%d/%d", $id, $this->fightSimulator->getSeed()));

                return $this->renderFight($opponentA, $opponentB, $deckDB->getName(), $randomPreset->name);
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/fight/testArena/{id}/{otherId}/{seed}", name="fightInArena")
     *
     * @param int $id
     * @param int $otherId
     * @param int $seed
     *
     * @return Response
     */
    public function fightArena(int $id, int $otherId, int $seed = 0): Response
    {
        /** @var User $user */
        if ($user = $this->getUser()) {
            /** @var DeckRepository $deckRepository */
            $deckRepository = $this->managerRegistry->getManager()->getRepository(DeckDB::class);

            // Case 1 : No Seed, ckeck if we own the deck ($id) to trigger the fight.
            if (0 === $seed) {
                $deckDB = $user->findDeck($id);
            } else { // Case 2 : There is a seed, anyone can replay the fight!
                $deckDB = $deckRepository->find($id);
            }

            if ($deckDB) {
                if ($opponentDeckDB = $deckRepository->find($otherId)) {
                    $this->shouldSaveReplay = true;
                    $this->shouldShowResultPageLink = true;

                    $this->initializeFight($seed);

                    $opponentA = $this->fightSimulator->prepareOpponentWithDeck($user->getDisplayName(), $deckDB);
                    $opponentB = $this->fightSimulator->prepareOpponentWithDeck($opponentDeckDB->getOwner()->getDisplayName(), $opponentDeckDB);

                    $this->fightSimulator->setFightUrl(sprintf("/fight/showReplay/%d/%d/%d", $id, $otherId, $this->fightSimulator->getSeed()));

                    return $this->renderFight($opponentA, $opponentB, $deckDB->getName(), $opponentDeckDB->getName());
                }
            }
        }

        return $this->redirectToRoute('homepage');
    }


    /**
     * @Route("/fight/showReplay/{id}/{otherId}/{seed}", name="showReplay")
     *
     * @param int $id
     * @param int $otherId
     * @param int $seed
     *
     * @return Response
     */
    public function showReplay(int $id, int $otherId, int $seed): Response
    {
        /** @var DeckRepository $deckRepository */
        $deckRepository = $this->managerRegistry->getManager()->getRepository(DeckDB::class);

        $deckDB = $deckRepository->find($id);

        if ($deckDB) {
            if ($opponentDeckDB = $deckRepository->find($otherId)) {
                $this->initializeFight($seed);

                $opponentA = $this->fightSimulator->prepareOpponentWithDeck($deckDB->getOwner()->getDisplayName(), $deckDB);
                $opponentB = $this->fightSimulator->prepareOpponentWithDeck($opponentDeckDB->getOwner()->getDisplayName(), $opponentDeckDB);

                $this->fightSimulator->setFightUrl(sprintf("/fight/showReplay/%d/%d/%d", $id, $otherId, $this->fightSimulator->getSeed()));

                return $this->renderFight($opponentA, $opponentB, $deckDB->getName(), $opponentDeckDB->getName());
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/fight/result/{replayId}", name="result")
     *
     * @param int $replayId
     *
     * @return Response
     */
    public function renderFightResult(int $replayId): Response
    {
        if ($replayRepository = $this->managerRegistry->getRepository(Replay::class)) {
            $replay = $replayRepository->findOneBy(['id' => $replayId]);

            if ($replay) {
                return $this->render('result.html.twig', ['replay' => $replay]);
            }
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * Initializes the fight with a random seed or the specified seed
     * @param int $seed
     */
    private function initializeFight(int $seed): void
    {
        if (0 === $seed) {
            $this->fightSimulator->randomizeSeed();
        } else {
            $this->fightSimulator->setSeed($seed);
        }

        if ($this::DUMP_SEED) {
            dump(sprintf("Seed : %d", $this->fightSimulator->getSeed()));
        }
    }

    /**
     * Simulates the fight and returns the view with the simulation data
     * @param Opponent $opponentA
     * @param Opponent $opponentB
     * @param string   $nameA
     * @param string   $nameB
     *
     * @return Response
     */
    private function renderFight(Opponent $opponentA, Opponent $opponentB, string $nameA, string $nameB): Response
    {
        $this->fightSimulator->prepareFight($opponentA, $opponentB);
        $this->fightSimulator->simulateFight();

        $params = [
            "versus" => sprintf("%s VS %s", $nameA, $nameB),
            "logs" => $this->fightSimulator->getLogs(),
        ];

        if ($this->shouldSaveReplay) {
            $replay = $this->createReplayFromFight();

            //TODO Find a way to find the replay during showReplay
            //TODO Maybe by searching a replay with those decks/seed combination (But what about duplicates ?)
            if ($this->shouldShowResultPageLink) {
                $params["logs"][] = sprintf("Result Page : <a href=\"/fight/result/%s\">/fight/result/%s</a>", $replay->getId(), $replay->getId());
            }
        }

        return $this->render('fight.html.twig', $params);
    }

    /**
     * @return Replay|null
     */
    private function createReplayFromFight(): ?Replay
    {
        $idDeckA = $this->fightSimulator->getDuel()->getFirstOpponent()->getDeckId();
        $idDeckB = $this->fightSimulator->getDuel()->getSecondOpponent()->getDeckId();
        $won = $this->fightSimulator->getDuel()->getWinner() === $this->fightSimulator->getDuel()->getFirstOpponent();

        $deckRepository = $this->managerRegistry->getManager()->getRepository(DeckDB::class);
        if ($deckRepository) {
            $deckA = $deckRepository->find($idDeckA);
            $deckB = $deckRepository->find($idDeckB);
            if ($deckA && $deckB) {
                $replay = new Replay($deckA, $deckB, $this->fightSimulator->getSeed(), $won);
                $this->managerRegistry->getManager()->persist($replay);
                $this->managerRegistry->getManager()->flush();

                return $replay;
            }
        }

        return null;
    }
}
