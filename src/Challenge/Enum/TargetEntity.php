<?php

namespace App\Challenge\Enum;

/**
 * https://php.watch/versions/8.1/enums
 */
enum TargetEntity: string
{
    case Player = "player";
    case Dinoz  = "dinoz";
    case Egg    = "egg";
    case Item   = "item";
    case Charm  = "charm";

    /**
     * @param string|array $input
     * @return array
     */
    public static function parse(string|array $input): array
    {
        if (is_string($input)) {
            return array( TargetEntity::from($input));
        }
        $outputEntity = array();
        foreach ($input as $entity) {
            array_push($outputEntity, TargetEntity::from($entity));
        }
        return $outputEntity;
    }
}