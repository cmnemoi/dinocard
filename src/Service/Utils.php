<?php

namespace App\Service;

use App\Challenge\Enum\ElementType;

class Utils
{
    /**
     * @param int $len
     *
     * @return string
     */
    public static function getRandomWord($len = 10) : string
    {
        $word = array_merge(range('a', 'z'), range('A', 'Z'));
        shuffle($word);

        return substr(implode($word), 0, $len);
    }

    /**
     * Returns an dinocard energy as an image
     * @param string $element Should be an ElementType
     *
     * @return string
     */
    public static function getEnergyAsImage(string $element): string
    {
        return "<img src='/img/gfx/icons/energy_".$element.".gif'>";
    }
}