<?php

namespace App\Challenge\Entity;

use App\Challenge\Entity\EnergySystem\EnergyReserve;
use App\Challenge\Enum\ElementType;

/**
 */
class Opponent
{
    private string $name;
    private int $deckId = -1;
    
    private int $health;

    private CardStack $deck;

    /** @var array<Card> */
    private array $graveyard = [];

    private EnergyReserve $energyReserve;
    private Board $board;
    private bool $isImmuneToDeath = false;


    /**
     * @param string $name
     * @param int    $health
     */
    public function __construct(string $name, int $health = 10)
    {
        $this->name = $name;
        $this->health = $health;
        $this->deck = new CardStack();
        $this->energyReserve = new EnergyReserve();
        $this->board = new Board();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @param int $health
     */
    public function setHealth(int $health): void
    {
        $this->health = max(0, $health);
    }

    /**
     * @param int $mod
     */
    public function modifyHealth(int $mod): void
    {
        $this->setHealth($this->health + $mod);
    }

    /**
     * @return EnergyReserve
     */
    public function getEnergyReserve(): EnergyReserve
    {
        return $this->energyReserve;
    }

    /**
     * @param Card $card
     */
    public function addCardInEnergyReserve(Card $card): void
    {
        $this->energyReserve->addEnergySource($card);
    }

    /**
     * @param ElementType $element
     * @param int         $count
     *
     * @return array
     */
    public function removeEnergiesInReserve(ElementType $element, int $count) : array
    {
        $removedEnergySources = $this->energyReserve->removeEnergyByElement($element, $count);

        foreach ($removedEnergySources as $energySource) {
            if ($energySource instanceof Card) {
                $this->addCardInGraveyard($energySource);
            }
        }

        return $removedEnergySources;
    }

    /**
     * @param int $count
     *
     * @return array
     */
    public function removeRandomEnergiesInReserve(int $count) : array
    {
        $allowedElements = [];
        foreach (ElementType::cases() as $elementType) {
            if (ElementType::Neutral !== $elementType) {
                for ($e = 0; $e < $this->getEnergyCount($elementType); $e++) {
                    $allowedElements[] = $elementType;
                }
            }
        }

        $removedEnergySources = [];
        for ($i = 0; $i < $count && count($allowedElements) > 0; $i++) {
            $elementIndex = array_rand($allowedElements);
            $elementToRemove = $allowedElements[$elementIndex];
            array_splice($allowedElements, $elementIndex, 1);
            $removedEnergy = $this->removeEnergiesInReserve($elementToRemove, 1);
            $removedEnergySources = array_merge($removedEnergySources, $removedEnergy);
        }

        return $removedEnergySources;
    }

    /**
     * Returns true if this Opponent has enough energy to play the specified card
     * @param Card $card
     *
     * @return bool
     */
    public function hasEnoughEnergy(Card $card): bool
    {
        $remainingEnergyPool = $this->getTotalEnergyCount();
        $hasEnoughEnergy = true;
        foreach ($card->getCosts() as $cost) {
            if ($cost->getElement() === ElementType::Neutral) {
                $hasEnoughEnergy = $remainingEnergyPool >= $cost->getCount();
            } else {
                if ($this->getEnergyCount($cost->getElement()) >= $cost->getCount()) {
                    $remainingEnergyPool -= $cost->getCount();
                } else {
                    $hasEnoughEnergy = false;
                }
            }

            if (!$hasEnoughEnergy) {
                break;
            }
        }

        return $hasEnoughEnergy;
    }

    /**
     * @param ElementType $energy
     *
     * @return int
     */
    public function getEnergyCount(ElementType $energy) : int
    {
        return $this->energyReserve->getEnergyCount($energy);
    }

    /**
     * @return int
     */
    public function getTotalEnergyCount() : int
    {
        return $this->energyReserve->getTotalEnergyCount();
    }

    /**
     * @return CardStack
     */
    public function getDeck(): CardStack
    {
        return $this->deck;
    }

    /**
     * @return array<Card>
     */
    public function getGraveyardCards(): array
    {
        return $this->graveyard;
    }

    /**
     * @param ?Card $card
     */
    public function addCardInGraveyard(?Card $card): void
    {
        if (null !== $card) {
            $this->graveyard[] = $card;
        }
    }

    /**
     */
    public function clearGraveyards(): void
    {
        $this->graveyard = [];
    }

    /**
     * @param callable $filter
     *
     * @return array
     */
    public function filterGraveyardCards(callable $filter): array
    {
        return array_filter($this->graveyard, $filter);
    }

    public function removeCardFromGraveyard(int $cardIndex): void
    {
        array_splice($this->graveyard, $cardIndex);
    }

    /**
     * @param int $amount
     */
    public function discardTopDeck(int $amount): void
    {
        for ($i = 0; $i < $amount; $i++) {
            $card = $this->getDeck()->drawCard();
            $this->addCardInGraveyard($card);
        }
    }

    /**
     * @return Board
     */
    public function getBoard(): Board
    {
        return $this->board;
    }

    /**
     * @return bool
     */
    public function isImmuneToDeath(): bool
    {
        return $this->isImmuneToDeath;
    }

    /**
     * @param bool $isImmuneToDeath
     */
    public function setIsImmuneToDeath(bool $isImmuneToDeath): void
    {
        $this->isImmuneToDeath = $isImmuneToDeath;
    }

    /**
     * @return bool
     */
    public function isEliminated(): bool
    {
        if ($this->isImmuneToDeath()) {
            return false;
        }

        return $this->health <= 0;
    }

	/**
	 * @return int
	 */
	public function getDeckId(): int {
		return $this->deckId;
	}
	
	/**
	 * @param int $deckId 
	 */
	public function setDeckId(int $deckId) {
		$this->deckId = $deckId;
	}
}
