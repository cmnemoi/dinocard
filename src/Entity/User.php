<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * User id from eternal twin, he should be unique
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private string $userId;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private string $displayName;

    /**
     * @ORM\Column(type="json")
     */
    private array $roles = [];

    /**
     * One User has many decks. This is the inverse side.
     * @ORM\OneToMany(targetEntity="DeckDB", mappedBy="owner")
     *
     * @var Collection<DeckDB>
     */
    private Collection $decks;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private \DateTime $lastConnection;

    /* GAMEPLAY */

    public function __construct(string $userId, string $displayName)
    {
        $this->userId = $userId;
        $this->displayName = $displayName;
        $this->lastConnection = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(string $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getDisplayName(): ?string
    {
        return $this->displayName;
    }

    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->displayName;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword() : ?string
    {
        // not needed for apps that do not check user passwords
        return null;
    }

    /**
     * @see UserInterface
     */
    public function getSalt() : ?string
    {
        // not needed for apps that do not check user passwords
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    /**
     * @return \DateTime
     */
    public function getLastConnection(): \DateTime
    {
        return $this->lastConnection;
    }

    /**
     * @param \DateTime $lastConnection
     */
    public function setLastConnectionDate(\DateTime $lastConnection)
    {
        $this->lastConnection = $lastConnection;
    }

    /**
     * @return Collection<int, DeckDB>
     */
    public function getDecks(): Collection
    {
        return $this->decks;
    }

    public function findDeck(int $id): ?DeckDB
    {
        $result = null;

        /** @var DeckDB $deck */
        foreach ($this->decks as $deck) {
            if ($deck->getID() === $id) {
                $result = $deck;
                break;
            }
        }

        return $result;
    }

    public function hasAnyDeck(): bool
    {
        return count($this->decks) > 0;
    }

    public function getFirstDeck(): ?DeckDB
    {
        if ($this->hasAnyDeck()) {
            return $this->decks[0];
        }

        return null;
    }

    public function getPoints(): int
    {
        $result = 0;
        foreach ($this->decks as $deck) {
            $result += $deck->getLevel();
        }

        return $result;
    }
}
