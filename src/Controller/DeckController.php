<?php

namespace App\Controller;

use App\Entity\CardDB;
use App\Entity\DeckDB;
use App\Entity\User;
use App\Repository\DeckRepository;
use Doctrine\ORM\Cache\Exception\FeatureNotImplemented;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * API that allow player to interact with its deck.
 */
class DeckController extends AbstractController
{
    private ManagerRegistry $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * Show the specified deck
     *
     * @Route("/deck/show/{id}", name="showDeck")*
     *
     * @param int $id
     *
     * @return Response
     */
    public function showDeck(int $id): Response
    {
        $deckDb = $this->findOwnedDeck($id);
        if (null === $deckDb) {
            /** @var DeckRepository $deckRepository */
            $deckRepository = $this->managerRegistry->getManager()->getRepository(DeckDB::class);
            $deckDb = $deckRepository->find($id);
        }

        if (null !== $deckDb) {
            return $this->render('Deck/showDeck.html.twig', ['deckDB' => $deckDb]);
        }

        return $this->render('index.html.twig');
    }

    /**
     * @Route("/deck/new", name="newDeck")
     *
     * @return Response
     */
    public function newDeck(): Response
    {
        return $this->render('Deck/newDeck.html.twig');
    }

    /**
     * @Route("/deck/saveNew", name="saveNewDeck", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function saveNewDeck(Request $request): Response
    {
        $payload = $request->request;

        /** @var User $user */
        if ($user = $this->getUser()) {
            $deckName = $payload->get('name');
            if ("" !== $deckName) {
                $deck = new DeckDB($user, $deckName);

                $deckCards = $payload->get('packCards');
                $cardsInfo = explode(' ', $deckCards);
                foreach ($cardsInfo as $cardInfo) {
                    $cardInfoArray = explode('#', $cardInfo);

                    $amount = min($cardInfoArray[1], 3);
                    $deck->addCard(new CardDB($cardInfoArray[0], $amount));
                }

                $this->managerRegistry->getManager()->persist($deck);
                $this->managerRegistry->getManager()->flush();

                return $this->redirectToRoute('showDeck', ['id' => $deck->getId()]);
            }
        }

        return $this->redirectToRoute('newDeck');
    }

    /**
     * @Route("/deck/edit/{id}", name="editDeck")
     *
     * @param int $id
     *
     * @return Response
     * @throws \Exception
     */
    public function editDeck(int $id): Response
    {
        throw new \Exception("Edit is not implemented");
    }

    /**
     * @Route("/deck/delete/{id}", name="deleteDeck", methods={"POST"})
     *
     * @param int $id
     *
     * @return Response
     */
    public function deleteDeck(int $id): Response
    {
        if ($deckDb = $this->findOwnedDeck($id)) {
            $this->managerRegistry->getManager()->remove($deckDb);
            $this->managerRegistry->getManager()->flush();
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @param int $id
     *
     * @return DeckDB|null
     */
    private function findOwnedDeck(int $id): ?DeckDB
    {
        $result = null;
        /** @var User $user */
        if ($user = $this->getUser()) {
            if ($deck = $user->findDeck($id)) {
                $result = $deck;
            }
        }

        return $result;
    }

    /**
     * @Route("/deck/togglePrivate/{id}", name="togglePrivate")
     * 
     * @param int $id
     *
     * @return Response
     */
    public function togglePrivate($id): Response
    {
        if ($user = $this->getUser()) {
            if ($deck = $user->findDeck($id)) {
                $deck->togglePrivate();
                $this->managerRegistry->getManager()->flush();
            }
        }
        return $this->redirectToRoute('showDeck', ["id" => $id]);
    }
}