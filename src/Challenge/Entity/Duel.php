<?php

namespace App\Challenge\Entity;

/**
 * A challenge is
 */
class Duel
{
    private int $turnCount = 1;
    private Opponent $firstOpponent;
    private Opponent $secondOpponent;

    private ?Opponent $currentOpponent = null;

    private bool $noDrawAvailable = false;
    private int $turnSinceNoDrawAvailable = 0;

    /**
     * @param Opponent $opponentA
     * @param Opponent $opponentB
     */
    public function __construct(Opponent $opponentA, Opponent $opponentB)
    {
        $this->firstOpponent = $opponentA;
        $this->secondOpponent = $opponentB;
    }

    /**
     * @return int
     */
    public function getTurnCount(): int
    {
        return $this->turnCount;
    }

    /**
     *
     */
    public function incrementTurnCount(): void
    {
        $this->turnCount++;
        if ($this->getNoDrawAvailable()) {
            $this->turnSinceNoDrawAvailable++;
        }
    }

    // Dinoz attacks -> in front of him
    // Attack system needs to know Dinoz / Players
    /**
     * @return Opponent|null
     */
    public function getFirstOpponent(): ?Opponent
    {
        return $this->firstOpponent;
    }

    /**
     * @return Opponent|null
     */
    public function getSecondOpponent(): ?Opponent
    {
        return $this->secondOpponent;
    }

    /**
     * @return bool
     */
    public function isFirstOpponentTurn(): bool
    {
        return $this->getCurrentOpponent() === $this->getFirstOpponent();
    }

    /**
     * @return bool
     */
    public function getNoDrawAvailable(): bool
    {
        return $this->noDrawAvailable;
    }

    public function setNoDrawAvailable(bool $value)
    {
        $this->noDrawAvailable = $value;
    }

    /**
     * @return bool
     */
    public function getTurnSinceNoDrawAvailable(): int
    {
        return $this->turnSinceNoDrawAvailable;
    }

    public function setTurnSinceNoDrawAvailable(int $value)
    {
        $this->turnSinceNoDrawAvailable = $value;
    }

    /**
     * @return Opponent|null
     */
    public function getCurrentOpponent(): ?Opponent
    {
        return $this->currentOpponent;
    }

    /**
     * @return Opponent
     */
    public function getEnemyOpponent(): Opponent
    {
        return $this->isFirstOpponentTurn() ? $this->getSecondOpponent() : $this->getFirstOpponent();
    }

    /**
     * null -> A -> B -> A ...
     */
    public function cycleCurrentOpponent(): void
    {
        $this->currentOpponent = $this->getEnemyOpponent();
    }

    /**
     *
     */
    public function startFight(): void
    {
        $this->getFirstOpponent()->getDeck()->shuffle();
        $this->getSecondOpponent()->getDeck()->shuffle();
    }

    /**
     * @return bool
     */
    public function isFinished() : bool
    {
        $playerEliminated = $this->getFirstOpponent()->isEliminated() || $this->getSecondOpponent()->isEliminated();
        $playerCantDrawForTooLong = $this->turnSinceNoDrawAvailable >= 10;
        $duelTooLong = $this->turnCount > 100;

        return ($playerEliminated || $playerCantDrawForTooLong || $duelTooLong);
    }

    /**
     * Returns who won, return null if nobody won.
     * @return Opponent|null
     */
    public function getWinner() : ?Opponent
    {
        $winner = null;
        if ($this->isFinished()) {
            $firstEliminated = $this->getFirstOpponent()->isEliminated();
            $secondEliminated = $this->getSecondOpponent()->isEliminated();

            if ($firstEliminated || $secondEliminated) {
                if ($firstEliminated !== $secondEliminated) {
                    if ($firstEliminated) {
                        $winner = $this->getSecondOpponent();
                    } else {
                        $winner = $this->getFirstOpponent();
                    }
                }
            }

            if (null === $winner) {
                $firstHP = $this->getFirstOpponent()->getHealth();
                $secondHP = $this->getSecondOpponent()->getHealth();

                if ($firstHP !== $secondHP) {
                    if ($firstHP > $secondHP) {
                        $winner = $this->getFirstOpponent();
                    } else {
                        $winner = $this->getSecondOpponent();
                    }
                }
            }
        }

        return $winner;
    }

    /**
     * @return bool
     */
    public function isAnyDinozOnBattleField(): bool
    {
        return $this->getFirstOpponent()->getBoard()->hasDinoz() || $this->getSecondOpponent()->getBoard()->hasDinoz();
    }
}
